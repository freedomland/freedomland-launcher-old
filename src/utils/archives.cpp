/*
 * Code found here, hope you didn't mind ;)
 *  http://people.salixos.org/icaroperseo/14.1/xap/zeal/zeal.github/src/core
*/

#include "archives.h"

#include <QDir>
#include <QFileInfo>

Archives::Archives(QObject *parent, const QString &filename, const QString &destination) :
    QObject(parent),
    mHasErrorb(false),
    mFilename(filename),
    mDestination(destination),
    mAbort(false){}

void Archives::abort()
{
    mAbort = true;
}

bool Archives::wasAborted()
{
    return mAbort;
}

int Archives::getError() const
{
    return mError;
}

QString Archives::getErrorString() const
{
    return mErrorString;
}

bool Archives::hasError() const
{
    return mHasErrorb;
}

void Archives::progressCallback(void *ptr)
{
    ExtractInfo *info = reinterpret_cast<ExtractInfo *>(ptr);

    const qint64 extractedBytes = archive_filter_bytes(info->archiveHandle, -1);
    if (extractedBytes == info->extractedBytes)
        return;

    info->extractedBytes = extractedBytes;

    emit info->extractor->progress(static_cast<int>(extractedBytes * 100 / info->totalBytes));
}

void Archives::extract()
{
    ExtractInfo info;
    info.extractor = this;
    info.archiveHandle = archive_read_new();
    info.filePath = this->mFilename;
    info.totalBytes = QFileInfo(this->mFilename).size();
    info.extractedBytes = 0;

    archive_read_support_filter_all(info.archiveHandle);
    archive_read_support_format_all(info.archiveHandle);

    int r = archive_read_open_filename(info.archiveHandle, qPrintable(mFilename), 10240);
    if (r)
    {
        mErrorString = QString::fromLocal8Bit(archive_error_string(info.archiveHandle));
        mHasErrorb = true;
        mError = r;
        return;
    }

    archive_read_extract_set_progress_callback(info.archiveHandle, &Archives::progressCallback,
                                               &info);

    QDir destinationDir(mDestination);

    #ifdef Q_OS_WIN
    // make sense only on Default OS
    int flags = ARCHIVE_EXTRACT_TIME |
            ARCHIVE_EXTRACT_PERM | ARCHIVE_EXTRACT_ACL | ARCHIVE_EXTRACT_FFLAGS;
    #else
    int flags = 0;
    #endif

    archive_entry *entry;
    while (archive_read_next_header(info.archiveHandle, &entry) == ARCHIVE_OK && !mAbort)
    {
        QString pathname = archive_entry_pathname(entry);
        mFilesList.push_back(pathname);
        archive_entry_set_pathname(entry, qPrintable(destinationDir.absoluteFilePath(pathname)));
        archive_read_extract(info.archiveHandle, entry, flags);
    }

    emit completed();
    archive_read_free(info.archiveHandle);
}

QStringList Archives::getFilesList() const
{
    return mFilesList;
}
