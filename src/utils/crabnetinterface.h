#ifndef CRABNETINTERFACE_H
#define CRABNETINTERFACE_H

#include <QStringList>
#include <QObject>
#include <QTimer>

#define PING_UNREACHABLE 999

class CrabnetInterface : public QObject
{
    Q_OBJECT

    public:
        explicit CrabnetInterface(QObject *parent = nullptr);
        virtual ~CrabnetInterface();

        bool isOutdated() const;

        void startPingServer();
        void pausePingServer();

    signals:
        void becomeOnline();
        void becomeOffline();
        void ping(QString p);
        void players(QStringList p, bool outdated);

    private slots:
        void getStatus();

    private:
        unsigned int pingRakNetServer();
        //QStringList getPlayersList();

        bool mOutdated;
        bool mWasOnline;
        QStringList mLastList;
        QTimer *mPingTimer;
};

#endif // CRABNETINTERFACE_H
