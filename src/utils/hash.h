#ifndef MD5_H
#define MD5_H

#include <QFile>
#include <QString>

class Hash
{
    public:
        static QByteArray checkMd5(const QString &path);
        static QString calculateChecksum(const QString &path);

    private:
        Hash(){}
        ~Hash(){}
};

#endif // MD5_H
