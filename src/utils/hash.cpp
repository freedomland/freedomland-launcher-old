#include "hash.h"

#include <QCryptographicHash>
#include "third-party/qt-crc32/crc32.h"

QByteArray Hash::checkMd5(const QString &path)
{
    QByteArray currentHash;

    QFile f(path);
    if (f.open(QFile::ReadOnly))
    {
        QCryptographicHash hash(QCryptographicHash::Md5);
        if (hash.addData(&f))
        {
            return hash.result().toHex();
        }
    }

    return QByteArray();
}

QString Hash::calculateChecksum(const QString &path)
{
    Crc32 crc;
    qint64 checksum = crc.calculateFromFile(path);
    return QString().setNum(checksum, 16);
}
