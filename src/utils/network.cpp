#include "network.h"

#include <QEventLoop>
#include <QFile>
#include <QDataStream>

Network::Network(QObject *parent, const QUrl &url) :
    QObject (parent),
    mManager(new QNetworkAccessManager(this)),
    mUrl(url),
    mHasErrorb(false)
{
    mManager->setRedirectPolicy(QNetworkRequest::NoLessSafeRedirectPolicy);
    //reply->setReadBufferSize(1024);
}

Network::~Network()
{
    // just in case if user abort
    //delete mReply;

    delete mManager;
}

bool Network::hasError() const
{
    return mHasErrorb;
}

QString Network::errorString() const
{
    return mErrorStringb;
}

void Network::abort()
{
    mReply->abort();
}

QByteArray Network::download()
{
    mReply = mManager->get(QNetworkRequest(mUrl));

    connect(mReply, &QNetworkReply::downloadProgress, [&](qint64 bytesReceived, qint64 bytesTotal) {
        if (!mReply->error() && bytesTotal > 0)
            emit progress(static_cast<int>(bytesReceived * 100 / bytesTotal));
    });

    QEventLoop event;
    connect(mReply, &QNetworkReply::finished, &event, &QEventLoop::quit);
    event.exec();

    if (mReply->error() != QNetworkReply::NoError)
    {
        mHasErrorb = true;
        mErrorStringb = mReply->errorString();
        return QByteArray();
    }

    return mReply->readAll();
}

void Network::downloadFile(const QString &path)
{
    mReply = mManager->get(QNetworkRequest(mUrl));

    QFile file(path);
    if (file.open(QIODevice::WriteOnly))
    {
        connect(mReply, &QNetworkReply::readyRead, [&](){
            QByteArray b = mReply->readAll();
            QDataStream out(&file);
            out.writeRawData(b, b.size());
        });

        connect(mReply, &QNetworkReply::downloadProgress, [&](qint64 bytesReceived, qint64 bytesTotal) {
            if (!mReply->error())
                emit progress(static_cast<int>(bytesReceived * 100 / bytesTotal));
        });
    }

    QEventLoop event;
    connect(mReply, &QNetworkReply::finished, &event, &QEventLoop::quit);
    event.exec();

    if (mReply->error() != QNetworkReply::NoError)
    {
        mHasErrorb = true;
        mErrorStringb = mReply->errorString();
    }
}

void Network::postRequest(const QJsonObject &request)
{
    QNetworkRequest req(mUrl);
    req.setHeader(QNetworkRequest::ContentTypeHeader, "application/json");
    mReply = mManager->post(req, QJsonDocument(request).toJson());

    connect(mReply, &QNetworkReply::finished, [&](){
        qDebug() << "[POST] Reply received!";
        QByteArray response_data = mReply->readAll();
        QJsonDocument json = QJsonDocument::fromJson(response_data);
        emit finished(json);
    });
}
