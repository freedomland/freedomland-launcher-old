/*
 * Code found here, hope you didn't mind ;)
 *  http://people.salixos.org/icaroperseo/14.1/xap/zeal/zeal.github/src/core
*/

#ifndef ARCHIVES_H
#define ARCHIVES_H

#include <QString>
#include <QObject>
#include <archive.h>
#include <archive_entry.h>

class Archives : public QObject
{
    Q_OBJECT

    public:
        explicit Archives(QObject *parent = nullptr, const QString &filename = QString(), const QString &destination = QString());
        ~Archives(){}

        void extract();
        int getError() const;
        QString getErrorString() const;
        bool hasError() const;
        QStringList getFilesList() const;

        void abort();
        bool wasAborted();

    signals:
        void completed();
        void progress(double p);

    private:
        struct ExtractInfo
        {
            Archives *extractor;
            archive *archiveHandle;
            QString filePath;
            qint64 totalBytes;
            qint64 extractedBytes;
        };

        static void progressCallback(void *ptr);

        int mError;
        QString mErrorString;
        bool mHasErrorb;
        QString mFilename;
        QString mDestination;
        QStringList mFilesList;
        bool mAbort;
};

#endif // ARCHIVES_H
