#ifndef NETWORK_H
#define NETWORK_H

#include <QObject>
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QJsonDocument>

class Network : public QObject
{
    Q_OBJECT

    public:
        explicit Network(QObject *parent = nullptr, const QUrl &url = QUrl());
        ~Network();

        void downloadFile(const QString &path);
        QByteArray download();
        bool hasError() const;
        QString errorString() const;
        void postRequest(const QJsonObject &request);

    public slots:
        void abort();

    signals:
        void progress(int p);
        void aborted();
        void finished(QJsonDocument reply);

    private:
        QNetworkAccessManager *mManager;
        QNetworkReply *mReply;
        QUrl mUrl;

        bool mHasErrorb;
        QString mErrorStringb;
};

#endif // NETWORK_H
