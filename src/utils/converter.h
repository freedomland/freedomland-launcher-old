#ifndef UTILS_H
#define UTILS_H

#include <QString>

class Converter
{
    public:
        static QString convertSize(qint64 size);

    private:
        Converter(){}
};

#endif // UTILS_H
