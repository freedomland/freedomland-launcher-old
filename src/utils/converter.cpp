#include "src/utils/converter.h"

#include <QFileInfo>
#include <QtMath>

QString Converter::convertSize(qint64 size)
{
    QString ret;

    QString sizevalue;
    double currentsize = 0.0;

    if (size > qPow(1024, 8)) // 1 000 000 000 000 000 000 000 000 bytes
    {
        sizevalue = " YB";
        currentsize = double(size)/1024.0/1024.0/1024.0/1024.0/1024.0/1024.0/1024.0/1024.0;
    }
    else if (size > qPow(1024, 7)) // 1 000 000 000 000 000 000 000 bytes
    {
        sizevalue = " ZB";
        currentsize = double(size)/1024.0/1024.0/1024.0/1024.0/1024.0/1024.0/1024.0;
    }
    else if (size > qPow(1024, 6)) // 1 000 000 000 000 000 000 bytes
    {
        sizevalue = " EB";
        currentsize = double(size)/1024.0/1024.0/1024.0/1024.0/1024.0/1024.0;
    }
    else if (size > qPow(1024, 5)) // 1 000 000 000 000 000 bytes
    {
        sizevalue = " PB";
        currentsize = double(size)/1024.0/1024.0/1024.0/1024.0/1024.0;
    }
    else if (size > qPow(1024, 4)) // 1 000 000 000 000 bytes
    {
        sizevalue = " TB";
        currentsize = double(size)/1024.0/1024.0/1024.0/1024.0;
    }
    else if (size > qPow(1024, 3)) // 1 000 000 000 bytes
    {
        sizevalue = " GB";
        currentsize = double(size)/1024.0/1024.0/1024.0;
    }
    else if (size > qPow(1024, 2)) // 1 000 000 bytes
    {
        sizevalue = " MB";
        currentsize = double(size)/1024.0/1024.0;
    }
    else if (size > 1024) // 1 000 bytes
    {
        sizevalue = " kB";
        currentsize = double(size)/1024.0;
    }
    else if (size < 1024)
    {
        currentsize = size;
        sizevalue = " B";
    }

    ret = QString::number(currentsize, 'f', 1) + sizevalue;

    return ret;
}
