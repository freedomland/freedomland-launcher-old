#include "crabnetinterface.h"

#include <RakPeer.h>
#include <MessageIdentifiers.h>
#include <RakSleep.h>
#include <GetTime.h>

#include "src/core/paths.h"

CrabnetInterface::CrabnetInterface(QObject *parent) :
    QObject(parent),
    mOutdated(false),
    mWasOnline(false),
    mLastList({}),
    mPingTimer(new QTimer){}

CrabnetInterface::~CrabnetInterface()
{
    mPingTimer->stop();
    delete mPingTimer;
}

void CrabnetInterface::startPingServer()
{
    // allow client retrive information
    QTimer::singleShot(150, this, &CrabnetInterface::getStatus);

    connect(mPingTimer, &QTimer::timeout, this, &CrabnetInterface::getStatus);
    mPingTimer->start(5000);
}

void CrabnetInterface::pausePingServer()
{
    mPingTimer->stop();
}

void CrabnetInterface::getStatus()
{
    unsigned int p = pingRakNetServer();
    //QStringList list;

    //if (!mOutdated)
    //    list = getPlayersList();

    if (p >= PING_UNREACHABLE)
    {
        if (mWasOnline)
        {
            emit becomeOffline();
            mWasOnline = false;
        }
    }
    else
    {
        if (!mWasOnline)
            emit becomeOnline();

        /*if (mOutdated)
        {
            emit players({}, mOutdated);
        }
        else if (mLastList != list)
        {
            mLastList = list;
            emit players(list, mOutdated);
        }*/

        mWasOnline = true;
        emit ping(QString::number(p));
    }
}

// original code by Koncord
// https://github.com/TES3MP/openmw-tes3mp/blob/0.7.0/apps/browser/netutils/Utils.cpp
unsigned int CrabnetInterface::pingRakNetServer()
{
    RakNet::Packet *packet;
    bool done = false;
    RakNet::TimeMS time = PING_UNREACHABLE;

    RakNet::SocketDescriptor socketDescriptor{0, ""};
    RakNet::RakPeerInterface *peer = RakNet::RakPeerInterface::GetInstance();
    peer->Startup(1, &socketDescriptor, 1);

    if (!peer->Ping(UpdaterPath::realmIP, UpdaterPath::realmPort, false))
        return time;

    RakNet::TimeMS start = RakNet::GetTimeMS();

    while (!done)
    {
        RakNet::TimeMS now = RakNet::GetTimeMS();

        if (now - start >= PING_UNREACHABLE)
            break;

        packet = peer->Receive();

        if (!packet)
            continue;

        switch (packet->data[0])
        {
            case ID_DISCONNECTION_NOTIFICATION:
            case ID_CONNECTION_LOST:
                done = true;
                break;

            case ID_CONNECTED_PING:
            case ID_UNCONNECTED_PONG:
            {
                RakNet::BitStream bsIn(&packet->data[1], packet->length, false);
                bsIn.Read(time);
                time = now - time;
                done = true;
                break;
            }

            default:
                break;
        }

        peer->DeallocatePacket(packet);
    }

    peer->Shutdown(0);
    RakNet::RakPeerInterface::DestroyInstance(peer);
    return time > PING_UNREACHABLE ? PING_UNREACHABLE : time;
}

// original code by Koncord
// https://github.com/TES3MP/openmw-tes3mp/blob/0.7.0/apps/browser/netutils/Utils.cpp
/*QStringList CrabnetInterface::getPlayersList()
{
    QStringList players;
    RakNet::SocketDescriptor socketDescriptor = {0, ""};
    RakNet::RakPeerInterface *peer = RakNet::RakPeerInterface::GetInstance();
    peer->Startup(1, &socketDescriptor, 1);

    QString msg;

    if (peer->Connect(UpdaterPath::realmIP, UpdaterPath::realmPort, UpdaterPath::tes3mpServerVer,
                      UpdaterPath::tes3mpProtoVerReal, nullptr, 0, 3, 500, 0) != RakNet::CONNECTION_ATTEMPT_STARTED)
        msg = "Connection attempt failed.\n";

    int queue = 0;
    while (queue == 0)
    {
        for (RakNet::Packet *packet = peer->Receive(); packet; peer->DeallocatePacket(
                packet), packet = peer->Receive())
        {
            switch (packet->data[0])
            {
                case ID_CONNECTION_ATTEMPT_FAILED:
                {
                    //msg = "Connection failed.\n"
                    //        "Either the IP address is wrong or a firewall on either system is blocking\n"
                    //        "UDP packets on the port you have chosen.";
                    // Stay quiet, we already have ping, so user will find if server is down
                    queue = -1;
                    break;
                }
                case ID_INVALID_PASSWORD:
                {
                    //msg = "Connection failed."
                    //        "Server is outdated.";
                    mOutdated = true;
                    queue = -1;
                    break;
                }
                case ID_CONNECTION_REQUEST_ACCEPTED:
                {
                    //msg = "Connection accepted.\n";
                    queue = 1;
                    break;
                }
                case ID_DISCONNECTION_NOTIFICATION:
                    throw std::runtime_error("ID_DISCONNECTION_NOTIFICATION.\n");
                case ID_CONNECTION_BANNED:
                    throw std::runtime_error("ID_CONNECTION_BANNED.\n");
                case ID_CONNECTION_LOST:
                    throw std::runtime_error("ID_CONNECTION_LOST.\n");
                //default:
                    //qDebug() << QString("Connection message with identifier %1 has arrived in initialization.\n").arg(packet->data[0]);
            }
        }
    }

    if (queue == -1) // connection is failed
        return players;

    {
        RakNet::BitStream bs;
        bs.Write(static_cast<unsigned char>(ID_USER_PACKET_ENUM + 1));
        peer->Send(&bs, HIGH_PRIORITY, RELIABLE_ORDERED, 0, RakNet::UNASSIGNED_SYSTEM_ADDRESS, true);
    }

    RakNet::Packet *packet;
    bool done = false;
    while (!done) // I don't think we need this in the first place at all
    {
        for (packet = peer->Receive(); packet; peer->DeallocatePacket(packet), packet = peer->Receive())
        {
            if (packet->data[0] == (ID_USER_PACKET_ENUM + 1))
            {
                RakNet::BitStream bs(packet->data, packet->length, false);
                bs.IgnoreBytes(1);
                size_t length = 0;
                bs.Read(length);

                // isn't length here will be always 0?
                for (size_t i = 0; i < length; i++)
                {
                    RakNet::RakString str;
                    bs.Read(str);
                    players.push_back(str.C_String());
                }

                // Also, wtf? I don't get this.
                // So... We're readnig packages until...
                // Uh, I still don't get it
                done = true;
            }
        }
    }

    peer->Shutdown(0);
    RakSleep(10);
    RakNet::RakPeerInterface::DestroyInstance(peer);

    return players;
}*/

bool CrabnetInterface::isOutdated() const
{
    return mOutdated;
}
