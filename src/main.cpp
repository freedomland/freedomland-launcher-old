/*
    Freedomland Updater - update or install Freedom Land client setup
    Copyright (C) 2018 Volk_Milit (aka Ja'Virr-Dar)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <QApplication>
#include <QCommandLineParser>
#include <QDate>
#include <singleapplication.h>

#include "ui/newmainform.h"

#include "src/core/paths.h"

static void migrateSettings()
{
    QFile file(UpdaterPath::freedomlandcfg);
    if (!file.open(QIODevice::ReadWrite))
        return;

    if (file.size() == 0)
    {
        file.close();
        return;
    }

    const QByteArray &content = file.readAll();
    if (content.at(0) == '[')
        return;

    const QByteArray &result = "[General]\n" + content;

    file.write(result);
    file.close();
}

int main(int argc, char *argv[])
{
    SingleApplication app(argc, argv, true, SingleApplication::Mode::SecondaryNotification);

    if (app.isSecondary())
        return 0;

    QCoreApplication::setApplicationName("FreedomLand Launcher");
    QCoreApplication::setApplicationVersion("v" + UpdaterPath::programver);

    QCommandLineParser parser;
    parser.setApplicationDescription("FreedomLand Launcher (c) 2018-" + QString::number(QDate().year()) + " FreedomLand Team");
    parser.addHelpOption();
    parser.addVersionOption();

    parser.process(app);

    migrateSettings();

    NewMainForm form(nullptr);
    form.show();
    if (!form.prepare())
        exit(0);

    return app.exec();
}
