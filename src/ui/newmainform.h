#ifndef NEWMAINFORM_H
#define NEWMAINFORM_H

#include "ui_newmainform.h"

#include "src/ui/advancedsettingsdialog.h"

#include "src/core/settings.h"
#include "src/core/apiparser.h"

#include "src/utils/crabnetinterface.h"

#include <QTimer>

class NewMainForm : public QWidget
{
    Q_OBJECT

    public:
        explicit NewMainForm(QWidget *parent = nullptr);
        ~NewMainForm();
        bool prepare();

    private slots:
        void on_btn_settings_clicked();
        void on_btn_play_clicked();
        void on_btn_about_clicked();
        void updatePlayerList();
        void on_btn_vkcom_clicked();
        void on_btn_discord_clicked();
        void on_donateButton_clicked();
        void on_refreshPlayers_clicked();
        void apiStaff();

    signals:
        void aborted();

    protected:
        void closeEvent(QCloseEvent *e);

    private:
        Ui::NewMainForm *ui;
        LauncherSettings *mSettings;
        CrabnetInterface *mCNInterface;
        QTimer *mTimer;
        QTimer *mRefreshTimer;
        APIParser *mApi;

        bool mComplited;
        bool mDebug;
        bool mAborted;
        bool mStarted;
};

#endif // NEWMAINFORM_H
