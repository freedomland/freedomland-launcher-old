#ifndef INSTALLMASTERFORM_H
#define INSTALLMASTERFORM_H

#include <QDialog>

#include "ui_installmasterform.h"

#include "src/core/packages.h"

class InstallMasterForm : public QDialog
{
    Q_OBJECT

    public:
        explicit InstallMasterForm(QWidget *parent = nullptr);
        ~InstallMasterForm();

    private slots:
        void on_MainScreen_currentChanged(int arg1);
        void on_btn_next_clicked();
        void on_btn_prev_clicked();
        void on_lw_components_itemChanged(QListWidgetItem *item);
        void on_btn_cancel_clicked();
        void on_btn_done_clicked();
        void on_btn_searchformorrowind_clicked();
        void on_le_morrowind_install_textChanged(const QString &arg1);

        void on_btn_supersecret_clicked();

private:
        Ui::InstallMasterForm *ui;
        Packages *mPackages;

        void installPackages();
        void populateInstallsList();
        void populateComponentsList();

        bool mDoOnce;
        bool mPopulateDone;

        void page0(); // welcoming
        void page1(); // Morrowind
        void page2(); // packages
        void page3(); // install
        void page4(); // ending
};

#endif // INSTALLMASTERFORM_H
