#ifndef CUSTOMKEYSEQUENCE_H
#define CUSTOMKEYSEQUENCE_H

#include <QLineEdit>
#include <QKeyEvent>
#include <QList>

class CustomKeySequence : public QLineEdit
{
    Q_OBJECT

    public:
        explicit CustomKeySequence(QWidget *parent = nullptr);

    signals:
        void invalidKey(const QString &key);

    protected:
        void keyPressEvent(QKeyEvent *);

    private:
        QList<int> invalidKeys;
};

#endif // CUSTOMKEYSEQUENCE_H
