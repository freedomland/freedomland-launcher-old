#include "installitem.h"

InstallItem::InstallItem(const packet &packet) :
    QListWidgetItem()
{
    mPacket = packet;
}

packet InstallItem::getPacket() const
{
    return mPacket;
}
