#include "morrowindinstallationform.h"
#include "ui_morrowindinstallationform.h"

#include <QFile>
#include <QFileDialog>
#include <QDesktopServices>

#include "src/core/checksetup.h"
#include "src/core/settings.h"

#include "src/utils/network.h"
#include "src/utils/archives.h"

MorrowindInstallationForm::MorrowindInstallationForm(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::MorrowindInstallationForm),
    mDone(false)
{
    ui->setupUi(this);

    ui->buttonOk->setHidden(true);
    ui->buttonOk->setEnabled(false);
    ui->buttonBack->setHidden(true);
}

MorrowindInstallationForm::~MorrowindInstallationForm()
{
    delete ui;
}

void MorrowindInstallationForm::on_retailButton_clicked()
{
    ui->stackedWidget->setCurrentIndex(1);

    const QString &datafiles = CheckSetup().getDataFiles();

    if (datafiles.isEmpty())
    {
        ui->manualLabelFound->setHidden(true);
        ui->buttonOk->setHidden(true);
        mDone = false;
    }
    else
    {
        ui->manualLabelFound->setText(ui->manualLabelFound->text().append(datafiles));
        ui->manualLabelNotFound->setHidden(true);
        ui->buttonOk->setHidden(false);
        ui->buttonBack->setHidden(false);
        ui->buttonOk->setEnabled(true);

        morrowindInstallPath = datafiles;

        mDone = true;
    }
}

void MorrowindInstallationForm::on_pirateButton_clicked()
{
    ui->stackedWidget->setCurrentIndex(3);

    const QString &installpath = LauncherSettings().getInstallPath();

    Network net(nullptr, UpdaterPath::serverRoot + "/morrowind.zip");
    connect(&net, &Network::progress, ui->progressBar, &QProgressBar::setValue);
    net.downloadFile(installpath + "/morrowind.zip");

    Archives archives(nullptr, installpath + "/morrowind.zip", installpath + "/morrowind");
    connect(&archives, &Archives::progress, ui->progressBar, &QProgressBar::setValue);
    archives.extract();

    QFile(installpath + "/morrowind.zip").remove();

    morrowindInstallPath = installpath + "/morrowind";
    mDone = true;
    this->close();
}

void MorrowindInstallationForm::on_manualButton_clicked()
{
    ui->stackedWidget->setCurrentIndex(2);
    ui->buttonOk->setHidden(false);
    ui->buttonOk->setEnabled(false);
    ui->buttonBack->setHidden(false);
}

void MorrowindInstallationForm::on_buttonBack_clicked()
{
    ui->stackedWidget->setCurrentIndex(0);
}

void MorrowindInstallationForm::on_stackedWidget_currentChanged(int arg1)
{
    if (arg1 == 0)
    {
        ui->buttonOk->setHidden(true);
        ui->buttonBack->setHidden(true);
    }
}

void MorrowindInstallationForm::on_lineEdit_textChanged(const QString &arg1)
{
    if (arg1.isEmpty() || !QFile(ui->lineEdit->text()).exists() ||
            !ui->lineEdit->text().contains("Morrowind.esm"))
    {
        ui->buttonOk->setEnabled(false);
        mDone = false;
    }
    else
    {
        ui->buttonOk->setEnabled(true);
        morrowindInstallPath = ui->lineEdit->text().remove("/Morrowind.esm");
        mDone = true;
    }
}

void MorrowindInstallationForm::on_searchMorrowindButton_clicked()
{
    QString file = QFileDialog::getOpenFileName(this, "Путь до Morrowind.esm", QDir::homePath(), "Morrowind.esm");
    if (!file.isEmpty())
        ui->lineEdit->setText(file);
}

void MorrowindInstallationForm::on_buttonOk_clicked()
{
    this->close();
}

void MorrowindInstallationForm::on_buttonSteam_clicked()
{
    QDesktopServices::openUrl(QUrl("https://store.steampowered.com/app/22320/The_Elder_Scrolls_III_Morrowind_Game_of_the_Year_Edition/"));
}

void MorrowindInstallationForm::on_buttonGOG_clicked()
{
    QDesktopServices::openUrl(QUrl("https://www.gog.com/game/the_elder_scrolls_iii_morrowind_goty_edition"));
}

void MorrowindInstallationForm::on_buttonBethesda_clicked()
{
    QDesktopServices::openUrl(QUrl("https://bethesda.net/en/games/EL3GSTPCBG01"));
}

void MorrowindInstallationForm::closeEvent(QCloseEvent *e)
{
    if (!mDone)
        e->ignore();
    else
        LauncherSettings().setMorrowindPath(morrowindInstallPath);
}
