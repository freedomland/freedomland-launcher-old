#include "newsitem.h"

NewsItemWidget::NewsItemWidget(QWidget *parent, const QString &title_string, const QString &body_string, const QString &url) :
    QWidget (parent)
{
    // set font doesn't work in QListWidget
    title.setText("<b>" + title_string + "</b>");
    body.setText(body_string);
    read.setText(QString("<a href='%1'>Читать дальше<a/>").arg(url));
    read.setTextInteractionFlags(Qt::TextBrowserInteraction);
    read.setOpenExternalLinks(true);

    layout.addWidget(&title);
    layout.addWidget(&body);
    layout.addWidget(&read);

    this->setLayout(&layout);
    this->setStyleSheet("margin-left: 10px;");
}
