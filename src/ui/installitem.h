#ifndef INSTALLITEM_H
#define INSTALLITEM_H

#include <QListWidgetItem>

#include "src/core/packages.h"

class InstallItem : public QListWidgetItem
{
    public:
        InstallItem(const packet &packet);
        packet getPacket() const;

    private:
        packet mPacket;
};

#endif // INSTALLITEM_H
