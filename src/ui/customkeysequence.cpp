#include "customkeysequence.h"

#include <QKeySequence>
#include <QDebug>

CustomKeySequence::CustomKeySequence(QWidget *parent) :
    QLineEdit(parent)
{
    this->setPlaceholderText("Нажмите клавишу...");

    invalidKeys = {
        Qt::Key_Shift,
        Qt::Key_Control,
        Qt::Key_Alt,
        Qt::Key_Return,
        Qt::Key_Escape,
        Qt::Key_ScrollLock,
        Qt::Key_Print,
        Qt::Key_Space,
        Qt::Key_NumLock,
        Qt::Key_Up,
        Qt::Key_Down,
        Qt::Key_Left,
        Qt::Key_Right,
        Qt::Key_Backspace,
        96 // tilde, can't find it in Qt::Key_*
    };
}

void CustomKeySequence::keyPressEvent(QKeyEvent *event)
{
    if (invalidKeys.contains(event->key()))
    {
        emit invalidKey(QKeySequence(event->key()).toString());
        event->ignore();
    }
    else
    {
        this->clear();
        this->setText(QKeySequence(event->key()).toString());
    }
}
