#ifndef MORROWINDINSTALLATIONFORM_H
#define MORROWINDINSTALLATIONFORM_H

#include <QDialog>
#include <QCloseEvent>

namespace Ui {
class MorrowindInstallationForm;
}

class MorrowindInstallationForm : public QDialog
{
    Q_OBJECT

    public:
        explicit MorrowindInstallationForm(QWidget *parent = nullptr);
        ~MorrowindInstallationForm();

    private slots:
        void on_retailButton_clicked();
        void on_pirateButton_clicked();
        void on_manualButton_clicked();
        void on_buttonBack_clicked();
        void on_stackedWidget_currentChanged(int arg1);
        void on_lineEdit_textChanged(const QString &arg1);
        void on_searchMorrowindButton_clicked();
        void on_buttonOk_clicked();
        void on_buttonSteam_clicked();
        void on_buttonGOG_clicked();
        void on_buttonBethesda_clicked();

    protected:
        void closeEvent(QCloseEvent *e);

    private:
        Ui::MorrowindInstallationForm *ui;

        QString morrowindInstallPath;
        bool mDone;
};

#endif // MORROWINDINSTALLATIONFORM_H
