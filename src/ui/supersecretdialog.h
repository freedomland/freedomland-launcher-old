#ifndef SUPERSECRETDIALOG_H
#define SUPERSECRETDIALOG_H

#include <QDialog>

namespace Ui {
class SuperSecretDialog;
}

class SuperSecretDialog : public QDialog
{
    Q_OBJECT

    public:
        explicit SuperSecretDialog(QWidget *parent = nullptr);
        ~SuperSecretDialog();

    private slots:
        void on_btn_search_clicked();
        void on_btn_ok_clicked();

        void on_lineEdit_textChanged(const QString &arg1);

private:
        Ui::SuperSecretDialog *ui;
};

#endif // SUPERSECRETDIALOG_H
