#ifndef NEWSITEM_H
#define NEWSITEM_H

#include <QWidget>
#include <QLabel>
#include <QVBoxLayout>
#include <QPaintEvent>

class NewsItemWidget : public QWidget
{
    Q_OBJECT

    public:
        explicit NewsItemWidget(QWidget *parent = nullptr,
                          const QString &title_string = QString(),
                          const QString &body_string = QString(),
                          const QString &url = QString());
        virtual ~NewsItemWidget(){}

    private:
        QLabel title{this};
        QLabel body{this};
        QLabel read{this};
        QVBoxLayout layout{this};
};

#endif // NEWSITEM_H
