#include "progressform.h"
#include "ui_progressform.h"

ProgressForm::ProgressForm(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ProgressForm),
    mError(false)
{
    ui->setupUi(this);

    //ui->backtraceField->setHidden(true);
    ui->okButton->setHidden(true);

    // resize to new size, since two widgets are gone
    //resize(sizeHint());
    resize(550, 500);
}

ProgressForm::~ProgressForm()
{
    delete ui;
}

void ProgressForm::onErrorEmited(const QString &error)
{
    ui->progressBar->setHidden(true);
    ui->backtraceField->setHidden(false);
    ui->okButton->setHidden(false);

    ui->label->setText("Произошла непредвиденная ошибка в клиенте tes3mp, последний бектрейс:");

    ui->backtraceField->clear();
    ui->backtraceField->setText(error);

    this->resize(780, 496);

    mError = true;
}

void ProgressForm::setProgress(int p)
{
    if (p >= 100)
        this->close();

    ui->progressBar->setValue(p);
}

void ProgressForm::setTrace(QString line)
{
    ui->backtraceField->append(line);
}

void ProgressForm::closeEvent(QCloseEvent *event)
{
    if (ui->progressBar->value() != 100 && !mError)
        event->ignore();
    else
        event->accept();
}

void ProgressForm::on_okButton_clicked()
{
    this->close();
}
