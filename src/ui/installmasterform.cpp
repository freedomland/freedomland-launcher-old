#include "installmasterform.h"

#include <QStorageInfo>
#include <QDir>
#include <QFileDialog>
#include <QFileInfo>
#include <QMap>
#include <QDebug>

#include "src/core/checksetup.h"
#include "src/core/paths.h"
#include "src/core/settings.h"

#include "src/ui/installitem.h"
#include "src/ui/supersecretdialog.h"

#include "src/utils/converter.h"

InstallMasterForm::InstallMasterForm(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::InstallMasterForm),
    mPackages(new Packages),
    mDoOnce(false),
    mPopulateDone(false)
{
    ui->setupUi(this);

    this->setWindowModality(Qt::WindowModal);
    this->setAttribute(Qt::WA_DeleteOnClose);

    ui->btn_done->setHidden(true);

    connect(mPackages, &Packages::progress, ui->progressBar, &QProgressBar::setValue);

    ui->morrowindInstallPathFrame->setHidden(true);
    ui->pb_m_progress->setHidden(true);
}

InstallMasterForm::~InstallMasterForm()
{
    delete mPackages;
    delete ui;
}

void InstallMasterForm::installPackages()
{
    ui->btn_next->setEnabled(false);
    ui->btn_prev->setEnabled(false);

    for (int i = 0; i < ui->lw_components->count(); ++i)
    {
        InstallItem *item = static_cast<InstallItem*>(ui->lw_components->item(i));

        if (item->checkState() == Qt::Checked)
        {
            ui->l_task->setText("устанавливаю " + item->getPacket().visibleName);
            mPackages->downloadPackage(item->getPacket());
        }
    }
}

void InstallMasterForm::populateInstallsList()
{
    CheckSetup checksetup;
    connect(&checksetup, &CheckSetup::progress, ui->pb_m_progress, &QProgressBar::setValue);
    QStringList list = checksetup.getDataFiles();

    if (list.isEmpty())
    {
        ui->pb_m_progress->setHidden(true);
        ui->morrowindInstallPathFrame->setHidden(false);
        ui->l_status0->setText("Не найдено установок Morrowind. "
                               "Вам придётся скачать и установить его. Или указать путь до "
                               "Morrowind.esm. Если у вас ещё нет Morrowind, нажмите \"Далее\".");
    }
    else
    {
        ui->pb_m_progress->setHidden(true);
        ui->l_status0->setText("Найдена активная установка: " + list.first());
        // workaround\hack
        ui->le_morrowind_install->setText(list.first());
    }

    ui->btn_prev->setEnabled(true);
    ui->btn_cancel->setEnabled(true);

    mDoOnce = true;
}

void InstallMasterForm::populateComponentsList()
{
    ui->lw_components->clear();

    long installsize = 0;

    if (!mPopulateDone)
        mPackages->populatePackagesList();

    QMap<QString, packet>::const_iterator i;

    for (i = mPackages->getPacketsList().constBegin(); i != mPackages->getPacketsList().constEnd(); ++i)
    {
        // todo: de-hardcode (how?)
        if (!ui->le_morrowind_install->text().isEmpty() && i.value().fileName == "morrowind.zip")
            continue;

        // todo: optimize
        InstallItem *item = new InstallItem(i.value());

        item->setText(i.value().visibleName + " (" + Converter::convertSize(i.value().willBeDownloaded) + ")");

        if (!i.value().optional) // not optional
        {
            installsize += (i.value().willBeDownloaded + i.value().willBeInstalled);
            item->setCheckState(Qt::Checked);
            // prevent user from unchecking item
            item->setFlags(item->flags() & ~Qt::ItemIsEnabled);
        }
        else
        {
            item->setCheckState(Qt::Unchecked);
        }

        // enable morrowind goty item if user doesn't have morrowind at all
        if (ui->le_morrowind_install->text().isEmpty() && i.value().fileName == "morrowind.zip")
        {
            installsize += (i.value().willBeDownloaded + i.value().willBeInstalled);
            item->setCheckState(Qt::Checked);
            item->setFlags(item->flags() & ~Qt::ItemIsEnabled);
        }

        ui->lw_components->addItem(item);
    }

    // remember: QStorageInfo only allowing canonicalPath
    QStorageInfo info(QFileInfo(UpdaterPath::installpath).canonicalPath());

    ui->l_sizeavail->setText(Converter::convertSize(info.bytesFree()));
    ui->l_sizewillbeinstalled->setText(Converter::convertSize(installsize));

    mPopulateDone = true;
}

void InstallMasterForm::page0()
{
    ui->btn_prev->setEnabled(true);
    ui->btn_cancel->setEnabled(true);
    ui->l_currentpage->setText("Мастер установки, главная страница");
}

void InstallMasterForm::page1()
{
    ui->l_currentpage->setText("Мастер установки, Morrowind");

    if (!mDoOnce)
    {
        ui->pb_m_progress->setHidden(false);
        populateInstallsList();
    }
}

void InstallMasterForm::page2()
{
    ui->l_currentpage->setText("Мастер установки, опции");
    if (!mPopulateDone)
        populateComponentsList();
}

void InstallMasterForm::page3()
{
    LauncherSettings settings;

    if (!ui->le_morrowind_install->text().isEmpty())
        settings.setMorrowindPath(ui->le_morrowind_install->text().remove("/Morrowind.esm"));
    else
        settings.setMorrowindPath(UpdaterPath::installpath + "/Morrowind/Data Files");

    ui->l_currentpage->setText("Мастер установки, установка");
    installPackages();

    CheckSetup setup;
    connect(&setup, &CheckSetup::progress, ui->progressBar, &QProgressBar::value);
    setup.installCfg();

    ui->MainScreen->setCurrentIndex(ui->MainScreen->currentIndex()+1);
}

void InstallMasterForm::page4()
{
    mPackages->updateSettingsContent();
    ui->l_currentpage->setText("Мастер установки, завершение");

    ui->btn_next->setHidden(true);
    ui->btn_prev->setHidden(true);
    ui->btn_cancel->setHidden(true);
    ui->btn_done->setHidden(false);
}

void InstallMasterForm::on_MainScreen_currentChanged(int arg1)
{
    if (arg1 == 0)
        page0();
    else if (arg1 == 1)
        page1();
    else if (arg1 == 2)
        page2();
    else if (arg1 == 3)
        page3();
    else if (arg1 == 4)
        page4();

    if (arg1 == 0)
        ui->btn_prev->setEnabled(false);
    else if (arg1 != 2)
        ui->btn_prev->setEnabled(true);
}

void InstallMasterForm::on_btn_next_clicked()
{
    ui->MainScreen->setCurrentIndex(ui->MainScreen->currentIndex()+1);
}

void InstallMasterForm::on_btn_prev_clicked()
{
    ui->MainScreen->setCurrentIndex(ui->MainScreen->currentIndex()-1);
}

void InstallMasterForm::on_lw_components_itemChanged(QListWidgetItem *item)
{
    Q_UNUSED(item)

    long size = 0;
    for (int i = 0; i < ui->lw_components->count(); ++i)
    {
        InstallItem *it = static_cast<InstallItem*>(ui->lw_components->item(i));
        if (it->checkState() == Qt::Unchecked)
            continue;

        size += (it->getPacket().willBeInstalled + it->getPacket().willBeDownloaded);
    }

    ui->l_sizewillbeinstalled->setText(Converter::convertSize(size));

    QStorageInfo info(UpdaterPath::installpath);
    if (info.bytesFree() < size)
    {
        ui->btn_next->setEnabled(false);
    }
    else
    {
        ui->btn_next->setEnabled(true);
    }
}

void InstallMasterForm::on_btn_cancel_clicked()
{
    mPackages->abort();
    this->done(1);
}

void InstallMasterForm::on_btn_done_clicked()
{
    if (!mPackages->getHasErrors())
        this->done(1);

    this->done(0);
}

void InstallMasterForm::on_btn_searchformorrowind_clicked()
{
    QFileDialog dialog(this, "Выбирете путь к Morrowind.esm");
    dialog.setNameFilter("Morrowind.esm");
    dialog.selectNameFilter("Morrowind.esm");
    dialog.exec();

    QString file = dialog.selectedFiles().first();
    if (QFileInfo(file).isFile())
        ui->le_morrowind_install->setText(file);
}

void InstallMasterForm::on_le_morrowind_install_textChanged(const QString &arg1)
{
    if (arg1.isEmpty())
    {
        ui->btn_next->setEnabled(true);
        mPopulateDone = false;
        return;
    }

    if (QFileInfo(arg1).isFile())
        ui->btn_next->setEnabled(true);
    else
        ui->btn_next->setEnabled(false);

    mPopulateDone = false;
}

void InstallMasterForm::on_btn_supersecret_clicked()
{
    SuperSecretDialog dialog;
    dialog.exec();
}
