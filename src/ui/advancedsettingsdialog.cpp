#include "advancedsettingsdialog.h"

#include "ui_advancedsettingsdialog.h"

#include "src/core/paths.h"
#include "src/core/checksetup.h"

#include <QProcess>
#include <QMessageBox>
#include <QDateTime>
#include <QDirIterator>

AdvancedSettingsDialog::AdvancedSettingsDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::AdvancedSettingsDialog),
    mCNInterface(new CrabnetInterface),
    mOpenmwSettings(new OpenmwSettings),
    mLauncherSettings(new LauncherSettings),
    mMpSettings(new MpSettings)
{
    ui->setupUi(this);

    this->setModal(true);

    ui->cb_shaders->setChecked(mOpenmwSettings->getShadersEnabled());
    ui->cb_obj_nm->setChecked(mOpenmwSettings->getShadersObjNormals());
    ui->cb_terr_nm->setChecked(mOpenmwSettings->getShadersLandNormals());
    ui->cb_obj_sm->setChecked(mOpenmwSettings->getShadersObjSpecmap());
    ui->cb_terr_sm->setChecked(mOpenmwSettings->getShadersLandSpecmap());
    ui->sb_fps_killer->setValue(mOpenmwSettings->getCellsNum());
    ui->dsp_viewdistance->setValue(mOpenmwSettings->getViewDistance());
    ui->comb_ownercursor->setCurrentIndex(mOpenmwSettings->getOwnerCursor());
    ui->cb_sneaktoggle->setChecked(mOpenmwSettings->getSneakToggle());
    ui->cb_grabcursor->setChecked(mOpenmwSettings->getGrabCursor());
    ui->cb_weapparam->setChecked(mOpenmwSettings->getMeleeInfo());
    ui->cb_effecttime->setChecked(mOpenmwSettings->getEffectDuration());
    ui->cb_enchchance->setChecked(mOpenmwSettings->getEnchantChance());
    ui->cb_projdamage->setChecked(mOpenmwSettings->getProjectileDamage());
    ui->cb_tradersstaff->setChecked(mOpenmwSettings->getMerchantEquip());

    ui->cb_use_formule->setChecked(mLauncherSettings->getUseCellsFormule());
    ui->slider_viewdistance->setDisabled(mLauncherSettings->getUseCellsFormule());
    ui->dsp_viewdistance->setDisabled(mLauncherSettings->getUseCellsFormule());

    if (mMpSettings->getLogLevel() < 1)
        ui->cb_debug->setChecked(true);

    ui->dsb_chatdelay->setValue(mMpSettings->getDelay());
    chatHideSequence.setText(mMpSettings->getChatModeKey());
    chatWriteSequence.setText(mMpSettings->getSayKey());

    QTimer::singleShot(7000, this, [&]{
        ui->label_8->setHidden(true);
    });

    ui->pb_iniimporter->setHidden(true);

    ui->l_warn->setHidden(true);

    connect(&chatHideSequence, &CustomKeySequence::invalidKey, this, &AdvancedSettingsDialog::invalidKeyWarning);
    connect(&chatWriteSequence, &CustomKeySequence::invalidKey, this, &AdvancedSettingsDialog::invalidKeyWarning);
    ui->horizontalLayout_3->addWidget(&chatHideSequence);
    ui->horizontalLayout_5->addWidget(&chatWriteSequence);
}

AdvancedSettingsDialog::~AdvancedSettingsDialog()
{
    delete mCNInterface;
    delete mOpenmwSettings;
    delete mLauncherSettings;
    delete mMpSettings;
    delete ui;
}

void AdvancedSettingsDialog::invalidKeyWarning(const QString &key)
{
    QString str = "Недопустимая клавиша: %1. "
                  "TES3MP понимает только одиночные клавиши, все сочетания с Ctrl, Alt, Shift и тд. будут "
                  "сокращены до одинарных. Также невозможно использование некоторых специальных клавиш.";
    ui->l_warn->setText(str.arg(key));
    ui->l_warn->show();
}

void AdvancedSettingsDialog::on_buttonBox_accepted()
{
    mOpenmwSettings->setOwnerCursor(ui->comb_ownercursor->currentIndex());
    mOpenmwSettings->setSneakToggle(ui->cb_sneaktoggle->isChecked() ? true : false);
    mOpenmwSettings->setViewDistance(ui->dsp_viewdistance->value());
    mOpenmwSettings->setCellsNum(ui->sb_fps_killer->value());
    mOpenmwSettings->setShadersEnabled(ui->cb_shaders->isChecked() ? true : false);
    mOpenmwSettings->setShadersObjNormalsEnabled(ui->cb_obj_nm->isChecked() ? true : false);
    mOpenmwSettings->setShadersObjSpecmapEnabled(ui->cb_obj_sm->isChecked() ? true : false);
    mOpenmwSettings->setShadersLandNormalsEnabled(ui->cb_terr_nm->isChecked() ? true : false);
    mOpenmwSettings->setShadersLandSpecmapEnabled(ui->cb_terr_sm->isChecked() ? true : false);
    mOpenmwSettings->setGrabCursor(ui->cb_grabcursor->isChecked() ? true : false);
    mOpenmwSettings->setEnchantChance(ui->cb_enchchance->isChecked() ? true : false);
    mOpenmwSettings->setProjectileDamage(ui->cb_projdamage->isChecked() ? true : false);
    mOpenmwSettings->setMeleeInfo(ui->cb_weapparam->isChecked() ? true : false);
    mOpenmwSettings->setEffectDuration(ui->cb_effecttime->isChecked() ? true : false);
    mOpenmwSettings->setMerchantEquip(ui->cb_tradersstaff->isChecked() ? true : false);
    mMpSettings->setDelay(ui->dsb_chatdelay->value());
    mMpSettings->setChatModeKey(chatHideSequence.text());
    mMpSettings->setSayKey(chatWriteSequence.text());
    mMpSettings->setLogLevel(ui->cb_debug->isChecked() ? 0 : 4);
    mLauncherSettings->setUseCellsFormule(ui->cb_use_formule->isChecked() ? true : false);
}

void AdvancedSettingsDialog::on_buttonBox_rejected()
{
    this->close();
}

void AdvancedSettingsDialog::on_sb_fps_killer_valueChanged(int arg1)
{
    if (ui->cb_use_formule->isChecked())
    {
        double val = (8192 * arg1 - 1024) * 0.93;
        ui->slider_viewdistance->setValue(int(val));
    }
}

void AdvancedSettingsDialog::on_cb_use_formule_clicked(bool checked)
{
    ui->slider_viewdistance->setEnabled(!checked);

    if (checked)
    {
        double val = (8192 * ui->sb_fps_killer->value() - 1024) * 0.93;
        ui->slider_viewdistance->setValue(int(val));
    }
}

void AdvancedSettingsDialog::on_slider_viewdistance_valueChanged(int value)
{
    ui->dsp_viewdistance->setValue(double(value));
}

void AdvancedSettingsDialog::on_dsp_viewdistance_valueChanged(double arg1)
{
    ui->slider_viewdistance->setValue(int(arg1));
}

void AdvancedSettingsDialog::on_cb_shaders_stateChanged(int arg1)
{
    ui->label_9->setHidden(arg1);
    ui->frame_shaders->setEnabled(arg1);
}

void AdvancedSettingsDialog::on_btn_iniimporter_clicked()
{
    QDateTime date = QDateTime::currentDateTime();
    QString realDate = date.toString().remove(" ").remove(":").remove(".");
    QMessageBox msg(this);
    msg.setStandardButtons(QMessageBox::Ok | QMessageBox::Cancel);
    msg.setText(QString("Это действие перезапишет ваш текущий openmw.cfg, "
                        "он будет сохранён как openmw.cfg.%1. Продолжить?").arg(realDate));
    if (msg.exec() == QMessageBox::Cancel)
        return;

    QFile().rename(UpdaterPath::openmwcfg, UpdaterPath::omwpath + "/openmw.cfg." + realDate);

    ui->pb_iniimporter->setHidden(false);
    CheckSetup cs;
    connect(&cs, &CheckSetup::progress, [&](int p){
        ui->pb_iniimporter->setValue(p);
    });
    cs.installCfg();
}
