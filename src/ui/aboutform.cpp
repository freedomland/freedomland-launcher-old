#include "aboutform.h"
#include "ui_aboutform.h"

AboutForm::AboutForm(QWidget *parent, int page) :
    QDialog(parent),
    ui(new Ui::AboutForm)
{
    ui->setupUi(this);

    this->setWindowModality(Qt::WindowModal);

    ui->textEdit->setReadOnly(true);
    ui->textEdit_2->setReadOnly(true);

    ui->tabWidget->setCurrentIndex(page);
}

AboutForm::~AboutForm()
{
    delete ui;
}

void AboutForm::on_btn_ok_clicked()
{
    done(0);
}
