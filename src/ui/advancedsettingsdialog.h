#ifndef ADVANCEDSETTINGSDIALOG_H
#define ADVANCEDSETTINGSDIALOG_H

#include <QDialog>

#include "src/core/settings.h"
#include "src/utils/crabnetinterface.h"
#include "customkeysequence.h"

namespace Ui {
class AdvancedSettingsDialog;
}

class AdvancedSettingsDialog : public QDialog
{
    Q_OBJECT

    public:
        explicit AdvancedSettingsDialog(QWidget *parent = nullptr);
        ~AdvancedSettingsDialog();

    private slots:
        void on_buttonBox_accepted();
        void on_buttonBox_rejected();
        void on_sb_fps_killer_valueChanged(int arg1);
        void on_cb_use_formule_clicked(bool checked);
        void on_slider_viewdistance_valueChanged(int value);
        void on_cb_shaders_stateChanged(int arg1);
        void on_btn_iniimporter_clicked();
        void invalidKeyWarning(const QString &key);
        void on_dsp_viewdistance_valueChanged(double arg1);

private:
        Ui::AdvancedSettingsDialog *ui;
        CrabnetInterface *mCNInterface;
        OpenmwSettings *mOpenmwSettings;
        LauncherSettings *mLauncherSettings;
        MpSettings *mMpSettings;

        CustomKeySequence chatHideSequence{this};
        CustomKeySequence chatWriteSequence{this};
};

#endif // ADVANCEDSETTINGSDIALOG_H
