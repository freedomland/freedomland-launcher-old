#ifndef PROGRESSFORM_H
#define PROGRESSFORM_H

#include <QWidget>
#include <QCloseEvent>

namespace Ui {
class ProgressForm;
}

class ProgressForm : public QWidget
{
    Q_OBJECT

    public:
        explicit ProgressForm(QWidget *parent = nullptr);
        ~ProgressForm();

    public slots:
        void setProgress(int p);
        void setTrace(QString line);
        void onErrorEmited(const QString &error);

    protected:
        void closeEvent(QCloseEvent *event);

    private slots:
        void on_okButton_clicked();

    private:
        Ui::ProgressForm *ui;

        bool mError;
};

#endif // PROGRESSFORM_H
