#include "newmainform.h"

#include <QMessageBox>
#include <QOperatingSystemVersion>
#include <QDesktopServices>
#include <QJsonObject>
#include <QJsonArray>

#include "src/ui/aboutform.h"
#include "src/ui/newsitem.h"
#include "src/ui/progressform.h"
#include "src/ui/morrowindinstallationform.h"

#include "src/core/packages.h"
#include "src/core/apiparser.h"
#include "src/core/paths.h"
#include "src/core/processinvoker.h"
#include "src/core/checksetup.h"

#include "src/utils/network.h"

NewMainForm::NewMainForm(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::NewMainForm),
    mSettings(new LauncherSettings),
    mCNInterface(new CrabnetInterface),
    mTimer(new QTimer),
    mRefreshTimer(new QTimer),
    mApi(new APIParser),
    mComplited(false),
    mAborted(false),
    mStarted(false)
{
    ui->setupUi(this);

    #ifdef __WIN32
    if (QOperatingSystemVersion::current() < QOperatingSystemVersion::Windows7)
        QMessageBox::critical(this, "Windows XP, srsl?!", tr("Вы используете устаревшую операционную систему (ака Windows XP). "
                                                          "TES3MP не поддерживает и никогда не будет поддерживать эту ОС. Вы всё-ещё можете "
                                                          "попробовать продолжить, но только на свой страх и риск."), QMessageBox::Ok);
    #endif

    this->setGeometry(mSettings->getWindowGeometry());

    connect(mRefreshTimer, &QTimer::timeout, [&](){
                ui->refreshPlayers->setEnabled(true);
            });
}

NewMainForm::~NewMainForm()
{
    mSettings->setWindowGeometry(this->geometry());

    mTimer->stop();

    delete mApi;
    delete mTimer;
    delete mCNInterface;
    delete mSettings;
    delete ui;
}

void NewMainForm::updatePlayerList()
{
    ui->lw_players->clear();

    for (QJsonValue p : mApi->getPlayers())
        ui->lw_players->addItem(p.toString());

    ui->l_players->setText(QString("( %1 ):").arg(QString::number(ui->lw_players->count())));

    /*if (mApi->hasError())
    {
        ui->lw_players->setHidden(true);
        ui->label->setHidden(true);
        ui->l_players->setHidden(true);
        ui->label_2->setHidden(false);
    }*/
}

void NewMainForm::apiStaff()
{
    for (const APIParser::NewsItem &item : mApi->getNews())
    {
        NewsItemWidget *nitem = new NewsItemWidget(ui->lw_news, item.title, item.body, item.url);
        QListWidgetItem *lwitem = new QListWidgetItem(ui->lw_news);
        nitem->setMinimumWidth(150);
        lwitem->setData(Qt::UserRole, item.url);
        lwitem->setSizeHint(QSize(ui->lw_news->width(), nitem->sizeHint().height()));
        ui->lw_news->setItemWidget(lwitem, nitem);
    }

    // get players
    updatePlayerList();
}

bool NewMainForm::prepare()
{
    QDir docs(UpdaterPath::omwpath);
    if (!docs.exists())
        QDir().mkpath(UpdaterPath::omwpath);

    ui->label_2->setHidden(true);
    ui->pb_progress->setValue(10);

    // get api
    mApi->retriveApi();
    connect(mApi, &APIParser::finished, this, &NewMainForm::apiStaff);

    // get ping
    mCNInterface->startPingServer();

    connect(mCNInterface, &CrabnetInterface::becomeOnline, [&]()
    {
        ui->l_ping->setHidden(false);
        ui->l_ping_status->setHidden(false);

        ui->l_status->setText(QStringLiteral("<font color='#38AC00'>online</font>"));

        if (mComplited)
            ui->btn_play->setEnabled(true);
    });

    connect(mCNInterface, &CrabnetInterface::becomeOffline, [&]()
    {
        ui->l_ping->setHidden(true);
        ui->l_ping_status->setHidden(true);

        ui->l_status->setText(QStringLiteral("<font color='#AC1400'>offline</font>"));

        if (mComplited)
            ui->btn_play->setEnabled(false);
    });

    connect(mCNInterface, &CrabnetInterface::ping, ui->l_ping, &QLabel::setText);

    if (mSettings->getInstallPath().isEmpty())
    {
        QDir dir(QApplication::applicationDirPath());
        dir.cdUp();
        mSettings->setInstallPath(dir.path());
    }

    Packages packages(nullptr, mApi);
    // update launcher
    connect(this, &NewMainForm::aborted, &packages, &Packages::abort);

    if (mApi->getLauncherVersion() > UpdaterPath::programver)
    {
        connect(&packages, &Packages::progress, ui->pb_progress, &QProgressBar::setValue);
        ui->l_task->setText("Скачиваю обновление лаунчера...");

        if (packages.updateProgramm())
            return false;
    }

    if (QFile(QApplication::applicationDirPath() + "/installedpackages.list").exists())
    {
        QFile::copy(QApplication::applicationDirPath() + "/installedpackages.list", UpdaterPath::omwpath + "/installedpackages.list");
        QFile::remove(QApplication::applicationDirPath() + "/installedpackages.list");
    }

    if (mSettings->getMorrowindPath().isNull())
    {
        MorrowindInstallationForm *form = new MorrowindInstallationForm(this);
        form->exec();

        CheckSetup().installCfg();
    }

    // update packages
    if (!mSettings->getMorrowindPath().isNull())
    {
        ui->pb_progress->setValue(50);

        connect(&packages, &Packages::progress, ui->pb_progress, &QProgressBar::setValue);

        connect(&packages, &Packages::updating, this, [&](QString package){
            ui->l_task->setText("Обновляю: " + package.toLower());
        });

        packages.checkForUpdates();

        QList<QString> errors = packages.getErrorArchives();

        if (!errors.isEmpty())
        {
            QMessageBox::warning(this, "Ошибка", "Следующие пакеты не прошли проверку целостности:\n" +
                               errors.join("\n") + "\n\nУстановка была отменена.", QMessageBox::Ok);
        }
        else
        {
            mComplited = packages.checkInstallation();
        }

        ui->btn_settings->setEnabled(mComplited);
        ui->btn_play->setEnabled(mComplited);
    }

    ui->pb_progress->setValue(100);

    // everything done
    ui->l_task->setText("Готово.");

    if (mSettings->getVersion() != UpdaterPath::programver)
    {
        AboutForm *unf = new AboutForm(this, 1);
        unf->exec();
        mSettings->setVersion(UpdaterPath::programver);
    }

    // user was press close button or alt+f4
    if (mAborted)
        return false;

    return true;
}

void NewMainForm::on_btn_settings_clicked()
{
    AdvancedSettingsDialog mSettingsdialog(this);
    mSettingsdialog.exec();
}

void NewMainForm::on_btn_play_clicked()
{
    OpenmwContent content;
    if (content.getContent()["content"].isEmpty())
    {
        MorrowindInstallationForm form(this);
        form.show();
        return;
    }

    if (mComplited)
    {  
        ProcessInvoker *invoker = new ProcessInvoker(this, mApi);
        ProgressForm *form = new ProgressForm;
        connect(invoker, &ProcessInvoker::progress, form, &ProgressForm::setProgress);
        connect(invoker, &ProcessInvoker::emitTrace, form, &ProgressForm::setTrace);
        connect(invoker, &ProcessInvoker::error, form, &ProgressForm::onErrorEmited);
        form->show();

        connect(invoker, &ProcessInvoker::started, [&](){
            ui->btn_play->setEnabled(false);
            mComplited = false; // prevent button enabled from ping update
            mStarted = true;
            showMinimized();
            mCNInterface->pausePingServer();
        });

        connect(invoker, &ProcessInvoker::finished, [&](){
            ui->btn_play->setEnabled(true);
            mComplited = true;
            mStarted = false;
            raise();
            mCNInterface->startPingServer();
        });

        invoker->startGame();
    }
}

void NewMainForm::on_btn_about_clicked()
{
    AboutForm form;
    form.exec();
}

void NewMainForm::on_btn_vkcom_clicked()
{
    QDesktopServices::openUrl(QUrl("https://vk.com/fl_mmo"));
}

void NewMainForm::on_btn_discord_clicked()
{
    QDesktopServices::openUrl(QUrl("https://discord.gg/VS6hnfz"));
}

void NewMainForm::closeEvent(QCloseEvent *e)
{
    if (mStarted == true)
    {
        QMessageBox::information(this, "tes3mp", "Вы не можете закрыть лаунчер, пока активен процесс игры. "
                                                 "Не пытайтесь завершить процесс, это может привести к краху клиента. "
                                                 "Это нужно для того, чтобы вы могли отправить отчёт о падении клиента нам. "
                                                 "Окно лаунчера будет свёрнуто, пока процесс tes3mp не завершится.", QMessageBox::Ok);
        showMinimized();
        e->ignore();
    }

    mAborted = true;
    emit aborted();
}

void NewMainForm::on_donateButton_clicked()
{
    QDesktopServices::openUrl(QUrl("https://www.donationalerts.com/r/freedomland"));
}

void NewMainForm::on_refreshPlayers_clicked()
{
    updatePlayerList();
    ui->refreshPlayers->setEnabled(false);
    mRefreshTimer->start(3000);
}
