#include "supersecretdialog.h"
#include "ui_supersecretdialog.h"

#include "src/core/paths.h"
#include "src/core/settings.h"

#include <QFileDialog>
#include <QStandardPaths>
#include <QApplication>

SuperSecretDialog::SuperSecretDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::SuperSecretDialog)
{
    ui->setupUi(this);

    this->setWindowModality(Qt::WindowModal);

    ui->lineEdit->setText(LauncherSettings().getInstallPath());

    ui->l_status->setHidden(true);
}

SuperSecretDialog::~SuperSecretDialog()
{
    delete ui;
}

void SuperSecretDialog::on_btn_search_clicked()
{
    QString path = QFileDialog::getExistingDirectory(this, "Путь для установки.", ui->lineEdit->text(), QFileDialog::ShowDirsOnly);

    if (!path.isEmpty())
    {
        ui->lineEdit->setText(path);
    }
}

void SuperSecretDialog::on_btn_ok_clicked()
{
    QString path = ui->lineEdit->text();

    if (path.isEmpty())
    {
        ui->l_status->setHidden(false);
        ui->l_status->setText("Нельзя указать пустой путь.");
        return;
    }

    if (!QDir(path).exists())
    {
        ui->l_status->setHidden(false);
        ui->l_status->setText("Путь не существует.");
        return;
    }

    if (!QFileInfo(path).isDir())
    {
        ui->l_status->setHidden(false);
        ui->l_status->setText("Путь не является директорией.");
        return;
    }

    if (!QFileInfo(path).isWritable())
    {
        ui->l_status->setHidden(false);
        ui->l_status->setText("Путь является защищённой директорией.");
        return;
    }

    if (ui->lineEdit->text().contains(qApp->applicationDirPath()))
    {
        ui->l_status->setHidden(false);
        ui->l_status->setText("Не устанавливайте ничего в директорию с лаунчером.");
        return;
    }

    #ifdef __WIN32
    if (path.contains("C:\Program Files"))
    {
        ui->l_status->setHidden(false);
        ui->l_status->setText("Установка в Program Files вызывает проблемы с tes3mp.");
        return;
    }
    #endif

    LauncherSettings settings;
    settings.setInstallPath(ui->lineEdit->text());

    UpdaterPath::installpath = ui->lineEdit->text();

    this->done(0);
}

void SuperSecretDialog::on_lineEdit_textChanged(const QString &arg1)
{
    if (arg1.isEmpty())
    {
        ui->lineEdit->setText(QStandardPaths::standardLocations(QStandardPaths::GenericDataLocation).first() + "/FreedomLand");
    }
}
