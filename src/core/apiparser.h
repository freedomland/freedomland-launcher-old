#ifndef APIPARSER_H
#define APIPARSER_H

#include <QString>
#include <QVector>
#include <QJsonObject>
#include <QJsonDocument>
#include <QMap>
#include <QObject>

class APIParser : public QObject
{
    Q_OBJECT

    public:
        APIParser(QObject *parent = nullptr);
        ~APIParser(){}

        struct NewsItem
        {
            QString title;
            QString body;
            QString url;
        };

        QString getLauncherVersion() const;
        QVector<NewsItem> getNews();
        QJsonArray getPlayers();
        QMap<QString, QJsonObject> getPackages();

        bool hasError() const;
        QString getErrorString() const;

        void retriveApi();

    signals:
        void finished();

    private:
        QJsonDocument currentApi;
};

#endif // APIPARSER_H
