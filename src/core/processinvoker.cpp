#include "processinvoker.h"

#include <QMessageBox>
#include <QDir>
#include <QTextStream>
#include <QVector>
#include <QDebug>
#include <QJsonArray>

#include "src/core/paths.h"
#include "src/core/settings.h"

ProcessInvoker::ProcessInvoker(QObject *parent, APIParser *api) :
    QObject(parent),
    mApi(api),
    proc(new QProcess){}

ProcessInvoker::~ProcessInvoker()
{
    delete proc;
}

void ProcessInvoker::startGame()
{
    QString formatString;
    QString content;
    QString archives;

    LauncherSettings settings;

    const QString &installpath = settings.getInstallPath();

    formatString += " --data=\"" + settings.getMorrowindPath() + "\"";

    for (QJsonValue val : mApi->getPackages()["data"]["datapath"].toArray())
        formatString += " --data=\"" + installpath + "/" + val.toString() + "\"";

    formatString += " --content=Morrowind.esm --content=Tribunal.esm --content=Bloodmoon.esm";

    for (QJsonValue val : mApi->getPackages()["data"]["conent"].toArray())
        content += " --content=" + val.toString();

    formatString += content;

    archives += " --fallback-archive=Morrowind.bsa --fallback-archive=Tribunal.bsa --fallback-archive=Bloodmoon.bsa";

    for (QJsonValue val : mApi->getPackages()["data"]["archives"].toArray())
        archives += " --fallback-archive=" + val.toString();

    formatString += archives;

    QDir::setCurrent(installpath + "/tes3mp");

    connect(proc, &QProcess::readyReadStandardOutput, this, &ProcessInvoker::readyRead);
    connect(proc, qOverload<int, QProcess::ExitStatus>(&QProcess::finished), this, &ProcessInvoker::message);

    qDebug() << "Launching: " << formatString;

    proc->start(installpath + "/tes3mp/tes3mp " + formatString + " --connect " + UpdaterPath::realm);

    emit started();
}

void ProcessInvoker::readyRead()
{
    proc->setReadChannel(QProcess::StandardOutput);
    QTextStream stream(proc);
    while (!stream.atEnd())
    {
        QString line = stream.readLine();
        log.push_back(line);

        emit emitTrace(line);

        if (line.contains("Commit"))
            emit progress(33);
        else if(line.contains("Received"))
            emit progress(66);
        else if(line.contains("OSG"))
            emit progress(99);
        else if(line.contains("Adding data"))
            emit progress(100);
    }
}

void ProcessInvoker::message(int)
{
    QString t;
    QVector<file> files;
    QString version;
    QString protoVersion;
    QString os;
    QStringList data;

    emit finished();

    for (int i = 0; i < log.count(); ++i)
    {
        if (log[i].contains("TES3MP client"))
        {
            QStringList tmp =log[i].split("(");
            version = tmp[0].split(" ")[2];
            os = tmp[1].remove(tmp[1].length() - 1, 1);
        }
        else if (log[i].contains("Protocol version:"))
        {
            protoVersion = log[i].split(": ")[1];
        }
        else if (log[i].contains("idx:"))
        {
            QStringList tmp = log[i].split(":");
            QString tmp_file = tmp[3].remove(0, 1);

            file f;
            f.checksum = "0x" + tmp[2].remove("\tfile").remove(0, 1);
            f.filepath = tmp_file;

            files.push_back(f);

            QStringList tmp_data = tmp_file.split("/");
            tmp_data.pop_back();
            data.push_back(tmp_data.join("/"));
        }
    }

    if (log.count() > 30)
        for(int i = log.count() - 19; i < log.count(); ++i)
            t.append(log[i] + "\n");
    else
        for (const QString &line : log)
            t.append(line + "\n");

    t.append("\n------------------------\n");
    t.append("Version: " + version + "\n");
    t.append("Protocol Verison: " + protoVersion + "\n");
    t.append("OS: " + os + "\n");
    t.append("------------------------\n\nLoaded plugins:\n");

    for (const file &f : files)
        t.append(f.filepath + " | " + f.checksum + "\n");

    t.append("\n------------------------\n\nData files:\n");

    data.removeDuplicates();

    for (const QString &d : data)
        t.append(d);

    if (t.contains("ERR", Qt::CaseInsensitive) || log.count() <= 12)
        emit error(t);
}

