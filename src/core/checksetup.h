#ifndef CHECKSETUP_H
#define CHECKSETUP_H

#include <QString>
#include <QObject>

class CheckSetup : public QObject
{
    Q_OBJECT

    public:
        explicit CheckSetup(QObject *parent = nullptr);
        QString getDataFiles();

        void installCfg();

    signals:
        void progress(int p);

    private:
        QString getDataFromOMW();
        bool isEsmChecksummsValid(const QString &path);
        QString validateINI(const QString &str) const;
};

#endif // CHECKSETUP_H
