﻿#include "packages.h"

#include <QFile>
#include <QDir>
#include <QJsonObject>
#include <QJsonArray>
#include <QJsonValue>
#include <QApplication>
#include <QSysInfo>
#include <QMessageBox>
#include <QTextStream>
#include <QJsonDocument>
#include <QDebug>
#include <QProcess>

#include "src/core/paths.h"
#include "src/core/settings.h"
#include "src/core/apiparser.h"

#include "src/utils/network.h"
#include "src/utils/archives.h"
#include "src/utils/hash.h"

Packages::Packages(QObject *parent, APIParser *api) :
    QObject(parent),
    mHasErrors(false),
    mAborted(false),
    mApi(api)
{
    #ifdef Q_OS_WIN
        if (QSysInfo::currentCpuArchitecture() == "x86_64")
            mPlatform = "win64";
        else
            platform = "win32";
    #else
        mPlatform = "linux";
    #endif
}

Packages::~Packages(){}

bool Packages::updateProgramm()
{
    QString file;

    if (mPlatform == "win64")
        file = "launcherupdate-win.exe";
    else if (mPlatform == "win32")
        file = "launcherupdate-win32.exe";
    else
        file = "launcherupdate-linux.sh";

    Network net(this, QUrl(UpdaterPath::serverRoot + "/" + file));
    connect(&net, &Network::progress, this, &Packages::progress);
    connect(this, &Packages::aborted, &net, &Network::abort);
    net.downloadFile(QApplication::applicationDirPath() + "/" + file);

    if (net.hasError())
    {
        QMessageBox::critical(nullptr, tr("Ошибка сети"), "Произошла ошибка во время скачивания новой версии лаунчера."
                                                             " Пожалуйста, сообщите следующую информацию разработчикам: " + net.errorString(),
                              QMessageBox::Ok);
        return false;
    }


    QProcess::startDetached(QApplication::applicationDirPath() + "/" + file);

    return true;
}

void Packages::abort()
{
    emit aborted();
    mAborted = true;
}

void Packages::removePackage(const QString &packageName)
{
    const QString &installpath = LauncherSettings().getInstallPath();

    QString fileContent;
    QFile file(UpdaterPath::installedPackagesPath);
    if (file.open(QIODevice::ReadOnly))
    {
        QTextStream in(&file);
        bool found = false;
        while (!in.atEnd())
        {
            QString line = in.readLine();

            if (found && line.isEmpty())
            {
                found = false;
                continue;
            }

            if (found)
            {
                QFileInfo info(installpath + "/" + line);
                if (!info.isDir())
                    QFile().remove(installpath + "/" + line);

                continue;
            }

            if (line[0] == "#" && line.split(":")[0] == "#" + packageName)
            {
                found = true;
                continue;
            }

            fileContent.append(line + "\n");
        }

        file.close();
    }

    if (file.open(QIODevice::WriteOnly | QIODevice::Truncate))
    {
        file.write(fileContent.toLatin1());
        file.close();
    }
}

void Packages::installPackage(const QString &packageName, const QString &installPath, const QStringList &filesList, const QString &version)
{
    // fuck qt json
    // new package format:
    // #package_name:version
    // file1
    // file2
    // file3
    // file4
    // <empty string, end of the package>

    QFile file(UpdaterPath::installedPackagesPath);
    if (file.open(QIODevice::WriteOnly | QIODevice::Append))
    {
        QTextStream in(&file);
        in << "#" << packageName << ":" << version << "\n";
        for (const QString &item : filesList)
        {
            in << installPath + "/" + item << "\n";
        }
        in << "\n";

        file.close();
    }
}

QList<QString> Packages::getErrorArchives()
{
    return mErrorsArchives;
}

void Packages::downloadPackage(const QString &packageName)
{
    QMap<QString, QJsonObject> packet = mApi->getPackages();
    QJsonObject currentPacket = packet[packageName];

    const QString &installpath = LauncherSettings().getInstallPath();

    if (!QDir(installpath).exists())
        QDir().mkpath(installpath);

    const QString &file = installpath + "/" + currentPacket["fileName"].toString();
    if (QFile(file).exists())
        QFile(file).remove();

    Network net(this, QUrl(UpdaterPath::server + currentPacket["fileName"].toString()));
    connect(&net, &Network::progress, this, &Packages::progress);
    connect(this, &Packages::aborted, &net, &Network::abort);
    net.downloadFile(installpath + "/" + currentPacket["fileName"].toString());

    if (mAborted)
        return;

    QByteArray hash = Hash::checkMd5(installpath + "/" + currentPacket["fileName"].toString());

    if (hash != currentPacket["hash"].toString())
    {
        mErrorsArchives.push_back(currentPacket["fileName"].toString());
        mHasErrors = true;
        return;
    }

    QDir installdir(installpath + "/" + currentPacket["unpackPath"].toString());
    if (!installdir.exists())
        QDir().mkpath(installpath + "/" + currentPacket["unpackPath"].toString());

    if (QFileInfo(currentPacket["fileName"].toString()).suffix() == "bsa")
    {
        // move file in right directory
        QFile().rename(installpath + "/" + currentPacket["fileName"].toString(),
                installpath + "/" + currentPacket["unpackPath"].toString() + "/" + currentPacket["fileName"].toString());
        installPackage(packageName,
                currentPacket["unpackPath"].toString(), {currentPacket["fileName"].toString()}, currentPacket["version"].toString());
        return;
    }

    Archives archive(this, file, installpath + "/" + currentPacket["unpackPath"].toString());
    connect(&archive, &Archives::progress, this, &Packages::progress);
    connect(this, &Packages::aborted, &archive, &Archives::abort);
    archive.extract();

    if (mAborted || archive.wasAborted())
        return;

    if (archive.hasError())
    {
        QMessageBox::critical(nullptr,
                              tr("Ошибка установки"),
                              tr("Произошла ошибка при распаковке пакета: ") + archive.getErrorString(),
                              QMessageBox::Ok);

        mHasErrors = true;

        return;
    }

    const QStringList &filesList = archive.getFilesList();
    installPackage(packageName, currentPacket["unpackPath"].toString(), filesList, currentPacket["version"].toString());

    QFile(file).remove();
}

void Packages::updatePackage(const QString &packageName)
{
    removePackage(packageName);
    QJsonObject currentPacket = mApi->getPackages()[packageName];
    emit updating(currentPacket["visibleName"].toString());
    downloadPackage(packageName);
}

void Packages::populateInstalledPackages()
{
    QFile installedpackagesfile(UpdaterPath::installedPackagesPath);
    if (installedpackagesfile.exists() && installedpackagesfile.open(QIODevice::ReadOnly))
    {
        QTextStream in(&installedpackagesfile);
        //in.setCodec("UTF-8");

        QString currentPackage;

        while (!in.atEnd())
        {
            QString line = in.readLine();

            if (line[0] == '#')
            {
                QStringList tmp = line.split(":");
                QString packageName = tmp[0].remove(0, 1);
                mInstalledList[packageName] = tmp[1];
                currentPackage = packageName;
            }
            else if (!line.isEmpty())
            {
                if (line.contains("packet"))
                    continue;

                mInstalledFiles[currentPackage].push_back(line);
            }
        }

        installedpackagesfile.close();
    }
}

bool Packages::getHasErrors() const
{
    return mHasErrors;
}

void Packages::checkForUpdates()
{
    populateInstalledPackages();

    QMap<QString, QJsonObject>::const_iterator i;

    for (i = mApi->getPackages().constBegin(); i != mApi->getPackages().constEnd(); ++i)
    {
        if (mAborted) // don't try to update anyting anymore
            break;

        if (mInstalledList[i.key()].isNull() && !i.value()["optional"].toBool() &&
                (i.value()["platform"].toString() == mPlatform || i.value()["platform"].toString() == "any") &&
                mInstalledList[i.key()] != i.value()["version"].toString())
        {
            qDebug() << "Updating " + i.key() + " from " + mInstalledList[i.key()] + " to " + i.value()["version"].toString();
            updatePackage(i.key());
            continue;
        }
    }
}

bool Packages::checkInstallation()
{
    const QString &installpath = LauncherSettings().getInstallPath();

    if (mInstalledFiles.isEmpty() ||
            !QFile(UpdaterPath::installedPackagesPath).exists())
        return false;

    QMap<QString, QStringList>::const_iterator i;

    for (i = mInstalledFiles.constBegin(); i != mInstalledFiles.constEnd(); ++i)
    {
        if (mAborted) // don't try to update anyting anymore
            break;

        for (const QString &f : i.value())
        {
            QFile file(installpath + "/" + f);
            if (!file.exists())
            {
                qDebug() << "[" + i.key() + "] file " + installpath + "/" + f + " not found, package will be redownload";
                updatePackage(i.key());
                break;
            }
        }
    }

    return true;
}
