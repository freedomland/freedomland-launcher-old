﻿#include "checksetup.h"

#include <QSettings>
#include <QFile>
#include <QTextStream>
#include <QProcess>
#include <QTextCodec>
#include <QApplication>
#include <QScreen>
#include <QMessageBox>

#include "src/core/paths.h"
#include "src/core/settings.h"

#include "src/utils/hash.h"
#include "src/utils/network.h"

#include "third-party/ftlip/QT/ftlip.h"

CheckSetup::CheckSetup(QObject *parent) :
    QObject (parent)
{}

QString CheckSetup::getDataFromOMW()
{
    QFile omwcfg(UpdaterPath::openmwcfg);
    if (!omwcfg.exists())
        return "";

    ftlip ini_omwcfg(UpdaterPath::openmwcfg);
    QStringList installs = ini_omwcfg.getMult("data");

    if (installs.isEmpty())
        return "";

    for (QString item : installs)
    {
        item.remove("\"");

        if (QFile(item + "/Morrowind.esm").exists())
        {
            return item;
        }
    }

    return "";
}

QString CheckSetup::getDataFiles()
{
    #ifdef __WIN64
        QSettings settings("HKEY_LOCAL_MACHINE\\Software\\Wow6432Node\\Bethesda Softworks\\Morrowind\\", QSettings::NativeFormat);
        const QString &val = settings.value("Installed Path").toString();
        if (!val.isEmpty())
            return val;
    #else
        QSettings settings("HKEY_LOCAL_MACHINE\\Software\\Bethesda Softworks\\Morrowind\\", QSettings::NativeFormat);
        const QString &val = settings.value("Installed Path").toString();
        if (!val.isEmpty())
            return val;
    #endif

    return getDataFromOMW();
}

bool CheckSetup::isEsmChecksummsValid(const QString &path)
{
    QString filename;
    QString ruscrc;
    QString engcrc;
    bool ok = false;

    filename = "Morrowind.esm";
    ruscrc = "7B6AF5B9";
    engcrc = "34282D67";

    if (QFile(path + "/Morrowind.esm").exists())
    {
        emit progress(50);

        if (Hash::calculateChecksum(path + "/Morrowind.esm") != ruscrc ||
                Hash::calculateChecksum(path + "/Morrowind.esm") != engcrc)
        {
            return true;
        }
    }
    else
    {
        emit progress(100);
        return false;
    }

    emit progress(100);

    return ok;
}

void CheckSetup::installCfg()
{
    LauncherSettings settings;
    QString morrowind = settings.getMorrowindPath().remove("/Data Files");

    emit progress(10);

    QFile omwcfg(UpdaterPath::openmwcfg);
    if (!omwcfg.exists())
    {
        // iniimporter can't create new directory (wut?)
        if (QDir().exists(UpdaterPath::omwpath))
            QDir().mkdir(UpdaterPath::omwpath);

        if (!QDir(morrowind + "/morrowind.ini").exists())
        {
            QMessageBox msg;
            msg.setIcon(QMessageBox::Critical);
            msg.setText("Невозможно найти morrowind.ini, неверная установка Morrowind?");
            msg.setStandardButtons(QMessageBox::Ok);
            emit progress(100);
            return;
        }

        emit progress(30);

        QProcess process;
        process.setProgram(LauncherSettings().getInstallPath() + "/tes3mp/openmw-iniimporter");
        process.setArguments({"-i", morrowind + "/Morrowind.ini", "-c", UpdaterPath::openmwcfg, "-e", "win1251", "-g"});
        process.start();
        process.waitForFinished();

        emit progress(60);

        // iniimporter for some reason doesn't create data keys
        if (omwcfg.open(QIODevice::WriteOnly | QIODevice::Append))
        {
            QTextStream in(&omwcfg);
            QByteArray e = "windows-1251";
            in.setCodec(QTextCodec::codecForName(e));
            in << "encoding=win1251\n\r";
            in << "data=\"" + morrowind + "/Data Files\"\n\r";

            omwcfg.close();
        }
    }

    emit progress(80);

    QFile omwsettings(UpdaterPath::openmwsettingscfg);
    if (!omwsettings.exists())
    {
        QScreen *rec = QGuiApplication::screens().first();

        OpenmwSettings settings;
        settings.setFullscreen(true);
        settings.setResulutionX(rec->size().width());
        settings.setResulutionY(rec->size().height());
        settings.setFiledOfView(60.0);
        settings.setCellsNum(1);
        settings.setViewDistance(6666.0);
        settings.setOwnerCursor(3);
        settings.setEffectDuration(true);
        settings.setMeleeInfo(true);
        settings.setEnchantChance(true);
        settings.setMerchantEquip(true);
        settings.setProjectileDamage(true);
    }

    MpSettings mpsettings;
    mpsettings.setLogLevel(4);

    emit progress(100);
}
