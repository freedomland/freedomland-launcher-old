#ifndef PATHS_H
#define PATHS_H

#include <QString>
#include <QDir>

namespace UpdaterPath
{
    extern QString omwpath;
    extern QString omwcontentpath;
    extern QString apiurl;
    extern QString newsapi;
    extern QString wallid;
    extern QString apikey;
    extern QString playersurl;
    extern QString server;
    extern QString serverRoot;
    extern QString pluginPrefix;
    extern QString freedomlandcfg;
    extern QString openmwcfg;
    extern QString programver;
    extern QString openmwsettingscfg;
    extern const char *realmIP;
    extern unsigned short realmPort;
    extern QString realm;
    extern const char *tes3mpServerVer;
    extern unsigned short tes3mpProtoVer;
    extern int tes3mpProtoVerReal;
    extern QString installedPackagesPath;
}

#endif // PATHS_H
