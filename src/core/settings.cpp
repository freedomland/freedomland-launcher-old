#include "settings.h"

void OpenmwSettings::setFullscreen(const bool &enable)
{
    setValue("Video/fullscreen", enable);
}

void OpenmwSettings::setResulutionX(const int &x)
{
    setValue("Video/resolution x", x);
}

void OpenmwSettings::setResulutionY(const int &y)
{
    setValue("Video/resolution y", y);
}

void OpenmwSettings::setFiledOfView(const double &fov)
{
    setValue("Camera/field of view", fov);
}

// -----------------

bool OpenmwSettings::getEnchantChance() const
{
    return value("Game/show enchant chance", true).toBool();
}

void OpenmwSettings::setEnchantChance(const bool &enable)
{
    setValue("Game/show enchant chance", enable);
}

bool OpenmwSettings::getProjectileDamage() const
{
    return value("Game/show projectile damage", true).toBool();
}

void OpenmwSettings::setProjectileDamage(const bool &enable)
{
    setValue("Game/show projectile damage", enable);
}

bool OpenmwSettings::getMeleeInfo() const
{
    return value("Game/show melee info", true).toBool();
}

void OpenmwSettings::setMeleeInfo(const bool &enable)
{
    setValue("Game/show melee info", enable);
}

bool OpenmwSettings::getEffectDuration() const
{
    return value("Game/show effect duration", true).toBool();
}

void OpenmwSettings::setEffectDuration(const bool &enable)
{
    setValue("Game/show effect duration", enable);
}

bool OpenmwSettings::getMerchantEquip() const
{
    return value("Game/prevent merchant equipping", true).toBool();
}

void OpenmwSettings::setMerchantEquip(const bool &enable)
{
    setValue("Game/prevent merchant equipping", enable);
}

bool OpenmwSettings::getShadersEnabled() const
{
    return value("Shaders/force shaders", true).toBool();
}

void OpenmwSettings::setShadersEnabled(const bool &enabled)
{
    setValue("Shaders/force shaders", enabled);
}

bool OpenmwSettings::getShadersObjNormals() const
{
    return value("Shaders/auto use object normal maps", true).toBool();
}

void OpenmwSettings::setShadersObjNormalsEnabled(const bool &enabled)
{
    setValue("Shaders/auto use object normal maps", enabled);
}

bool OpenmwSettings::getShadersLandNormals() const
{
    return value("Shaders/auto use terrain normal maps", true).toBool();
}

void OpenmwSettings::setShadersLandNormalsEnabled(const bool &enabled)
{
    setValue("Shaders/auto use terrain normal maps", enabled);
}

bool OpenmwSettings::getShadersObjSpecmap() const
{
    return value("Shaders/auto use object specular maps", true).toBool();
}

void OpenmwSettings::setShadersObjSpecmapEnabled(const bool &enabled)
{
    setValue("Shaders/auto use object specular maps", enabled);
}

bool OpenmwSettings::getShadersLandSpecmap() const
{
    return value("Shaders/auto use terrain specular maps", true).toBool();
}

void OpenmwSettings::setShadersLandSpecmapEnabled(const bool &enabled)
{
    setValue("Shaders/auto use terrain specular maps", enabled);
}

int OpenmwSettings::getCellsNum() const
{
    return value("Cells/exterior cell load distance", 1).toInt();
}

void OpenmwSettings::setCellsNum(const int &cells)
{
    setValue("Cells/exterior cell load distance", cells);
}

double OpenmwSettings::getViewDistance() const
{
    return value("Camera/viewing distance", 6666.0).toDouble();
}

void OpenmwSettings::setViewDistance(const double &range)
{
    setValue("Camera/viewing distance", range);
}

bool OpenmwSettings::getDisatanceLand() const
{
    return value("Terrain/distant terrain", false).toBool();
}

void OpenmwSettings::setDisatanceLand(const bool &enabled)
{
    setValue("Terrain/distant terrain", enabled);
}

int OpenmwSettings::getOwnerCursor() const
{
    return value("Game/show owned", 3).toInt();
}

void OpenmwSettings::setOwnerCursor(const int &level)
{
    setValue("Game/show owned", level);
}

bool OpenmwSettings::getSneakToggle() const
{
    return value("Input/toggle sneak", false).toBool();
}

void OpenmwSettings::setSneakToggle(const bool &enabled)
{
    setValue("Input/toggle sneak", enabled);
}

bool OpenmwSettings::getGrabCursor() const
{
    return value("Input/grab cursor", true).toBool();
}

void OpenmwSettings::setGrabCursor(const bool &enable)
{
    setValue("Input/grab cursor", enable);
}

LauncherSettings::LauncherSettings() : QSettings(
                                           UpdaterPath::freedomlandcfg,
                                           QSettings::IniFormat)
{}

// possible get rid of this settings?
QStringList LauncherSettings::getArchiveArray() const
{
    return value("fallback-archive").toString().split(";");
}

// possible get rid of this settings?
void LauncherSettings::setArchiveArray(const QStringList &array)
{
    QString result;

    for (const QString &archive : array)
        result += archive + ";";

    setValue("fallback-archive", result);
}

// possible get rid of this settings?
QStringList LauncherSettings::getContentArray() const
{
    return value("content").toString().split(";");
}

// possible get rid of this settings?
void LauncherSettings::setContentArray(const QStringList &array)
{
    QString result;

    for (const QString &archive : array)
        result += archive + ";";

    setValue("content", result);
}

// possible get rid of this settings?
QStringList LauncherSettings::getDataArray() const
{
    return value("data").toString().split(";");
}

// possible get rid of this settings?
void LauncherSettings::setDataArray(const QStringList &array)
{
    QString result;

    for (const QString &archive : array)
        result += archive + ";";

    setValue("data", result);
}

QString LauncherSettings::getInstallPath() const
{
    return value("installpath").toString();
}

void LauncherSettings::setInstallPath(const QString &path)
{
    setValue("installpath", path);
}

bool LauncherSettings::getUseCellsFormule() const
{
    return value("useCellsFormule", false).toBool();
}

void LauncherSettings::setUseCellsFormule(const bool &enabled)
{
    setValue("useCellsFormule", enabled);
}

QString LauncherSettings::getMorrowindPath() const
{
    return value("morrowindPath").toString();
}

void LauncherSettings::setMorrowindPath(const QString &path)
{
    setValue("morrowindPath", path);
}

QRect LauncherSettings::getWindowGeometry()
{
    return value("windowGeometry", QRect(195, 114, 913, 728)).toRect();
}

void LauncherSettings::setWindowGeometry(const QRect &geometry)
{
    setValue("windowGeometry", geometry);
}

QString LauncherSettings::getVersion() const
{
    return value("version").toString();
}

void LauncherSettings::setVersion(const QString &version)
{
    setValue("version", version);
}

MpSettings::MpSettings() : QSettings(
                               LauncherSettings().getInstallPath() + "/tes3mp/tes3mp-client-default.cfg",
                               QSettings::IniFormat)
{}

double MpSettings::getDelay() const
{
    return value("Chat/delay", 0.5).toDouble();
}

void MpSettings::setDelay(const double &delay)
{
    setValue("Chat/delay", delay);
}

QString MpSettings::getSayKey() const
{
    return value("Chat/keySay", "Y").toString();
}

void MpSettings::setSayKey(const QString &key)
{
    setValue("Chat/keySay", key);
}

QString MpSettings::getChatModeKey() const
{
    return value("Chat/keyChatMode", "F2").toString();
}

void MpSettings::setChatModeKey(const QString &key)
{
    setValue("Chat/keyChatMode", key);
}

int MpSettings::getLogLevel() const
{
    return value("logLevel", 4).toInt();
}

void MpSettings::setLogLevel(const int &level)
{
    setValue("logLevel", level);
}
