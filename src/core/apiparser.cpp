#include "apiparser.h"

#include <QJsonDocument>
#include <QJsonValue>
#include <QJsonArray>
#include <QDateTime>

#include "src/utils/network.h"
#include "src/core/paths.h"

APIParser::APIParser(QObject *parent) :
    QObject(parent){}

void APIParser::retriveApi()
{
    QJsonObject json;
    json.insert("multiply_request", true);
    QJsonArray req_list;
    req_list.push_back("get_players");
    req_list.push_back("get_news");
    req_list.push_back("get_packages");
    req_list.push_back("get_version");
    json.insert("requests_list", req_list);

    Network *net = new Network(nullptr, QUrl("http://127.0.0.1:25666"));
    net->postRequest(json);

    connect(net, &Network::finished, [&](QJsonDocument var){
        qDebug() << "[API] Recived API response";
        currentApi = var;
        emit finished();
    });
}

QJsonArray APIParser::getPlayers()
{
    QJsonArray playersArray = currentApi["players"].toArray();

    if (!playersArray.isEmpty())
        return playersArray;

    return {};
}

QVector<APIParser::NewsItem> APIParser::getNews()
{
    QVector<NewsItem> newsList;

    QJsonArray newsArray = currentApi["news"].toArray();

    if (!newsArray.isEmpty())
    {
        for (QJsonValue value : newsArray)
        {
            NewsItem item;
            item.title = value["date"].toString();
            item.body = value["text"].toString();
            item.url = value["url"].toString();

            newsList.push_back(item);
        }
    }
    else
    {
        NewsItem item;
        item.title = "Ошибка подключения к серверу";
        item.body = "Попробуйте позже";
        item.url = "";
        newsList.push_back(item);
    }

    return newsList;
}

QString APIParser::getLauncherVersion() const
{
    return currentApi["version"].toString();
}

QMap<QString, QJsonObject> APIParser::getPackages()
{
    QMap<QString, QJsonObject> ret;

    QJsonObject packages = currentApi["packages"].toObject();
    for (const QString &key : ret.keys())
    {
        ret.insert(key, packages[key].toObject());
    }

    return ret;
}
