#ifndef SETTINGS_H
#define SETTINGS_H

#include <QStringList>
#include <QString>
#include <QSize>
#include <QRect>
#include <QSettings>
#include <QTextStream>
#include <QDebug>

#include "src/core/paths.h"

class OMWSettingsBase
{
    public:
        OMWSettingsBase()
        {
            QFile file(UpdaterPath::openmwsettingscfg);
            file.open(QIODevice::ReadOnly);
            QTextStream in(&file);

            QString currentSection;

            while(!in.atEnd())
            {
                QString line = in.readLine();

                if (line.isEmpty())
                    continue;

                if (line[0] == '#')
                    continue;

                if (line[0] == '[')
                {
                    currentSection = line.remove('[').remove(']');
                    continue;
                }

                QStringList tmp = line.split("=");
                tmp[0] = tmp[0].simplified();
                tmp[1] = tmp[1].simplified();

                context[currentSection][tmp[0]] = tmp[1];
            }

            file.close();
        }

        virtual ~OMWSettingsBase()
        {
            QFile file(UpdaterPath::openmwsettingscfg);
            file.open(QIODevice::WriteOnly);
            file.seek(0);

            QByteArray f = "";

            QMap<QString, QMap<QString, QVariant>>::const_iterator i;

            for (i = context.constBegin(); i != context.constEnd(); ++i)
            {
                QString currentSection = i.key();
                QMap<QString, QVariant>::const_iterator tmp;

                f += "[" + currentSection + "]\r\n";

                for (tmp = context[currentSection].constBegin(); tmp != context[currentSection].constEnd(); ++tmp)
                {
                    f += tmp.key() + " = " + tmp.value().toString() + "\r\n";
                }

                f += "\r\n";
            }

            file.write(f);
            file.close();
        }

        QVariant value(const QString &key, QVariant defaultValue) const
        {
            QStringList tmp = key.split("/");

            if (context[tmp[0]].isEmpty() || !context[tmp[0]][tmp[1]].isValid())
                return defaultValue;

            return context[tmp[0]][tmp[1]];
        }

        void setValue(const QString &key, const QVariant &val)
        {
            QStringList tmp = key.split("/");
            context[tmp[0]][tmp[1]] = val;
        }

    private:
        QMap<QString, QMap<QString, QVariant>> context;
};

class OpenmwContent
{
    public:
        OpenmwContent()
        {
            QFile file(UpdaterPath::openmwcfg);

            if (file.open(QIODevice::ReadOnly))
            {
                QTextStream in(&file);

                while(!in.atEnd())
                {
                    QString line = in.readLine();
                    QStringList keyvalue = line.split("=");

                    QString key = keyvalue[0].simplified();

                    if (key[0] == '#')
                        continue;

                    QString value = keyvalue[1].simplified();

                    content[key].push_back(value);
                }
            }
            else
            {
                qDebug() << "Can't open openmw.cfg.";
            }
        }

        virtual ~OpenmwContent(){}

        QMap<QString, QStringList> getContent()
        {
            return content;
        }

    private:
        QMap<QString, QStringList> content;
};

class OpenmwSettings : private OMWSettingsBase
{
    public:
        OpenmwSettings(){}
        virtual ~OpenmwSettings(){}

        bool getShadersEnabled() const;
        void setShadersEnabled(const bool &enabled);
        bool getShadersObjNormals() const;
        void setShadersObjNormalsEnabled(const bool &enabled);
        bool getShadersLandNormals() const;
        void setShadersLandNormalsEnabled(const bool &enabled);
        bool getShadersObjSpecmap() const;
        void setShadersObjSpecmapEnabled(const bool &enabled);
        bool getShadersLandSpecmap() const;
        void setShadersLandSpecmapEnabled(const bool &enabled);
        int getCellsNum() const;
        void setCellsNum(const int &cells);
        double getViewDistance() const;
        void setViewDistance(const double &range);
        bool getDisatanceLand() const;
        void setDisatanceLand(const bool &enabled);
        int getOwnerCursor() const;
        void setOwnerCursor(const int &level);
        bool getSneakToggle() const;
        void setSneakToggle(const bool &enabled);
        bool getGrabCursor() const;
        void setGrabCursor(const bool &enable);
        bool getEnchantChance() const;
        void setEnchantChance(const bool &enable);
        bool getProjectileDamage() const;
        void setProjectileDamage(const bool &enable);
        bool getMeleeInfo() const;
        void setMeleeInfo(const bool &enable);
        bool getEffectDuration() const;
        void setEffectDuration(const bool &enable);
        bool getMerchantEquip() const;
        void setMerchantEquip(const bool &enable);

        void setFullscreen(const bool &enable);
        void setResulutionX(const int &x);
        void setResulutionY(const int &y);
        void setFiledOfView(const double &fov);
};

class LauncherSettings : private QSettings
{
    public:
        LauncherSettings();
        virtual ~LauncherSettings(){}

        QStringList getArchiveArray() const;
        void setArchiveArray(const QStringList &array);
        QStringList getContentArray() const;
        void setContentArray(const QStringList &array);
        QStringList getDataArray() const;
        void setDataArray(const QStringList &array);
        bool getUseCellsFormule() const;
        void setUseCellsFormule(const bool &enabled);
        QString getMorrowindPath() const;
        void setMorrowindPath(const QString &path);
        QRect getWindowGeometry();
        void setWindowGeometry(const QRect &geometry);
        QString getVersion() const;
        void setVersion(const QString &version);
        QString getInstallPath() const;
        void setInstallPath(const QString &path);
};

class MpSettings : private QSettings
{
    public:
        MpSettings();
        virtual ~MpSettings(){}

        int getLogLevel() const;
        // 0 -- verbose, 1 -- info, 2 -- warnings, 3 -- errors, 4 -- fatal errors only
        void setLogLevel(const int &level);
        QString getSayKey() const;
        void setSayKey(const QString &key);
        QString getChatModeKey() const;
        void setChatModeKey(const QString &key);
        double getDelay() const;
        void setDelay(const double &delay);
        QRect getChatGeometry();
        void setChatGeometry(QRect size);
};

#endif // SETTINGS_H
