#include "paths.h"

#include <QStandardPaths>

namespace UpdaterPath
{
#ifdef __WIN32
    QString omwpath = QStandardPaths::standardLocations(QStandardPaths::DocumentsLocation).first() + "/My Games/OpenMW";
#else
    QString omwpath = QStandardPaths::standardLocations(QStandardPaths::GenericConfigLocation).first() + "/openmw";
#endif

#ifdef __WIN32
    QString omwcontentpath = QStandardPaths::standardLocations(QStandardPaths::DocumentsLocation).first() + "/My Games/OpenMW";
#else
    QString omwcontentpath = QStandardPaths::standardLocations(QStandardPaths::GenericDataLocation).first() + "/openmw";
#endif

    QString openmwcfg = omwpath + "/openmw.cfg";
    QString freedomlandcfg = omwpath + "/freedomland.cfg";
    QString openmwsettingscfg = omwpath + "/settings.cfg";
    QString installedPackagesPath = omwpath + "/installedpackages.list";

    QString server = "http://185.139.69.21:25564/tes3mp/content/";
    QString serverRoot = "http://185.139.69.21:25564/tes3mp/";
    QString apiurl = "http://185.139.69.21:25666/";

    const char *realmIP = "185.139.69.21";
    unsigned short realmPort = 25565;
    QString realm = QString::fromLatin1(realmIP) + ":" + QString::number(realmPort);

    QString programver = "0.14";

    const char *tes3mpServerVer = "0.7.0-alpha";
    unsigned short tes3mpProtoVer = 7;
    // originaly there was:
    // std::stringstream sstr;
    // sstr << "0.7.0-alpha";
    // sstr << 7;
    // sstr.str().size(); // will be 12 for this strings
    // so we can make this Qt way
    int tes3mpProtoVerReal = QString(tes3mpServerVer).append(tes3mpProtoVer).size();
}
