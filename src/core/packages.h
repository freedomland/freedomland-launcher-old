#ifndef PACKAGES_H
#define PACKAGES_H

#include <QString>
#include <QByteArray>
#include <QList>
#include <QObject>
#include <QMap>
#include <QJsonObject>

#include "src/core/apiparser.h"

class Packages : public QObject
{
    Q_OBJECT

    public:
        explicit Packages(QObject *parent = nullptr, APIParser *api = nullptr);
        virtual ~Packages();

        void downloadPackage(const QString &packageName);
        void removePackage(const QString &packageName);
        void checkForUpdates();
        bool getHasErrors() const;
        bool updateProgramm();

        void updateSettingsContent();
        void populateInstalledPackages();

        bool checkInstallation();

        QList<QString> getErrorArchives();

        void abort();

    signals:
        void progress(int p);
        void updating(QString packagename);
        void aborted();

    private:
        QMap<QString, QString> mInstalledList;
        QMap<QString, QStringList> mInstalledFiles;
        bool mHasErrors;
        bool mAborted;

        APIParser *mApi;

        QList<QString> mErrorsArchives;

        void installPackage(const QString &packageName, const QString &installPath, const QStringList &filesList, const QString &verison);
        void updatePackage(const QString &packageName);

        void migrateList();

        QString mPlatform;
};

#endif // PACKAGES_H
