#ifndef PROCESSINVOKER_H
#define PROCESSINVOKER_H

#include <QObject>
#include <QProcess>
#include <QVector>

#include "src/core/apiparser.h"

class ProcessInvoker : public QObject
{
    Q_OBJECT

    public:
        explicit ProcessInvoker(QObject *parent = nullptr, APIParser *api = nullptr);
        virtual ~ProcessInvoker();

        void startGame();
        void startIniImporter();

    private slots:
        void readyRead();
        void message(int);

    signals:
        void started();
        void finished();
        void progress(int p);
        void emitTrace(QString line);
        void error(const QString &text);

    private:
        QVector<QString> log;

        APIParser *mApi;

        QProcess *proc;

        struct file
        {
            QString filepath;
            QString checksum;
        };
};

#endif // PROCESSINVOKER_H
