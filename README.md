# FreedomLand Launcher

Firm launcher writen special for FreedomLand server, using Qt, libarchive and ftlip (my custom ini-parser).

### Note

If you're readign this, I'm open source this sh... This usually means that server doesn't exist anymore, but it might be another reason. Anyway all credits going to FreedomLand. You CAN'T remove thouse credits and pass off this work as your own. People that create this thing:

- Volk_Milit (aka Ja'Virr-Dar) - programming
- Андрей 'Allen_White' Ерёма - testing
- Наталья 'Azumika' Кудрина - art

Also, note that launcher itself is far from ideal. I make it on my knee for about a week or so (this is my 4th attempt), it may or may not contains bugs, who knows.

Anyway, if you want to use this piece of... junk for your own server you must know this things:

- Server address and port is hardcoded in paths.cpp
- Program version hardcoded too and will compare to one in api
- We maintain our own text file format (yeah, another one) for packages, but also used json and (something like) ini (ftlip). So the file structure is:
	```
		#package:version
		package/files/path1
		package/files/path2
		package/files/path3
		
		#package2:version
		...
	```
Empty string means end of package.

Why I'm reinvent ~~ing~~ the ~~steel~~ wheel? Because for some reason QtJson can't write normal document, exept, it was writting many small 'documents' into one file. I was lazy and hurry for opening, so I'm doing this.

- Launcher doesn't change openmw.cfg (exept of creating it), but launch tes3mp with specific parametrs
- But launcher DOES change settings.cfg of openmw, so, beware!
- Launcher also can change tes3mp client config

Why I write so shitty piece of code? Well, I was hurry for server opening, so the code quality not so good. Feel free to change it, if you can.

### API

- Players api must return json array with players name
- Packages api must return json object with following structure:

```json
{
	"data": {
		"archives": ["example.bsa"], 
		"conent": ["example.esp"], 
		"datapath": ["example-data"]
	},
	
	"example-package": {
		"fileName": "example-esp.zip", 
		"hash": "1234567890abcdefge", 
		"optional": false, 
		"visibleName": "Example plugin", 
		"platform": "any", 
		"unpackPath": "example-data", 
		"version": "0.1"
	}
}
```

Where:
- data is a data using for unpack paths, also used as parametrs for launch tes3mp. OpenMW should know where to search for data files.
- example-package - first package, optional: false mean it's REQUIRE to install
- platform can be ether win32, win64 or linux

- News as for now get all recent post from Vkontakte wall, see [this documentation](https://vk.com/dev/wall.get)

### Build on Windows

- You need mingw and qtcreator to build it on Windows
- Launcher is 32bit and building so on Windows, but tes3mp itself doesn't support 32bit officially (even there is binary for it), anyway, launcher can download packages for any platform binary (win32\win64, linux, mac)
- You need to build [CrabNet](https://github.com/TES3MP/CrabNet) from TES3MP repo, just follow instructions
- You need to build [zlib](https://www.zlib.net/) on windows; No specific config, but keep static build
- You need also to compile [libarchive](https://github.com/libarchive/libarchive) by yourself; No, GnuWin32 is not an option, you need at least version 3.3.0

Here we go, you was sing to a quest. Sooooo, disable almost everything in project config of qtcreator. Enable zlib and acl (not sure it needed tho). Add new string called ZLIB_LIBRARY and add path to zlibstatic.a. In ZLIB_INCLUDES add path to zlib-1.2.11. Build.

- Build in installer using NSIS. cd to dist\windows directory, place all build libraries and exe in release folder, build FreedomLandInstaller.nsi

### Build on Linux

- Install QtCreator, GCC 6 (or later), libarchive
- You need also build [CrabNet](https://github.com/TES3MP/CrabNet) from TES3MP repo, just follow instructions
- Open pro file and build launcher
- cd in updater open pro file and build it
- That's it

Easy, right? Windows, you should be on shame.

### Third-party libraries

- [Crc32 calculation](https://github.com/nusov/qt-crc32) by nusov
- [SingleApplication](https://github.com/itay-grudev/SingleApplication) by itay-grudev

### License

GPL v3.0
