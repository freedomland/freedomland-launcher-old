;NSIS Modern User Interface
;Crow Translate Installer Script
;Written by Hennadii Chernyshchyk

!pragma warning error all

;--------------------------------
;Include Modern UI

  !include "MUI2.nsh"

;--------------------------------
;General

  ;App information and out file
  Name "Freedom Land"
  BrandingText "Freedom Land"
  !define VERSION "1.0"
  OutFile "FreedomLandInstaller.exe"
  
  ;Properly display all languages
  Unicode true

  ;Default installation folder
  InstallDir "D:\Games\FreedomLand"

  ;Request application privileges
  RequestExecutionLevel user
  
  ;Set compressor
  SetCompressor /SOLID lzma

;--------------------------------
;Variables

  Var StartMenuFolder
  
;--------------------------------
;Interface Settings

  !define MUI_ABORTWARNING
  
  ;Show all languages, despite user's codepage
  !define MUI_LANGDLL_ALLLANGUAGES  
  
  ;Set installer icon
  !define MUI_ICON "icon.ico"
  
  ;Welcome bitmaps
  !define MUI_WELCOMEFINISHPAGE_BITMAP "${NSISDIR}\Contrib\Graphics\Wizard\freedomland.bmp"
  !define MUI_UNWELCOMEFINISHPAGE_BITMAP "${NSISDIR}\Contrib\Graphics\Wizard\freedomland.bmp"

;--------------------------------
;Pages

  !define MUI_WELCOMEPAGE_TEXT "��� ��������� ��������� Freedom Land �� ��� ���������.$\r$\n$\r$\n������� ������ $\"�����$\" ��� �����������.$\r$\n$\r$\nArt by pink-reindeer [pink-reindeer.tumblr.com]"
  !insertmacro MUI_PAGE_WELCOME
  ;!insertmacro MUI_PAGE_LICENSE "..\..\COPYING"
  !insertmacro MUI_PAGE_DIRECTORY
    
  ;Start Menu Folder Page Configuration
  !define MUI_STARTMENUPAGE_REGISTRY_ROOT "HKCU" 
  !define MUI_STARTMENUPAGE_REGISTRY_KEY "Software\Freedom Land"
  !define MUI_STARTMENUPAGE_REGISTRY_VALUENAME "StartMenuFolder"
  !insertmacro MUI_PAGE_STARTMENU Application $StartMenuFolder
  
  !insertmacro MUI_PAGE_INSTFILES
  
  ;Launch App Page Configuration
  !define MUI_FINISHPAGE_RUN "$INSTDIR\FreedomLand.exe"
  !insertmacro MUI_PAGE_FINISH
  
  !insertmacro MUI_UNPAGE_WELCOME
  !insertmacro MUI_UNPAGE_CONFIRM
  !insertmacro MUI_UNPAGE_INSTFILES
  !insertmacro MUI_UNPAGE_FINISH
  
;--------------------------------
;Languages
 
  !insertmacro MUI_LANGUAGE "Russian"
  
;--------------------------------
;Reserve Files
  
  ;If you are using solid compression, files that are required before
  ;the actual installation should be stored first in the data block,
  ;because this will make your installer start faster.
  
  !insertmacro MUI_RESERVEFILE_LANGDLL

;--------------------------------
;Installer Sections

Section "Install"

  SetOutPath "$INSTDIR"
  
  ;Install Files (need to copy release folder to script directory)
  File /r "release\*"
  File "icon.ico"
  
  ;Store installation folder
  WriteRegStr HKCU "Software\Freedom Land" "" $INSTDIR
  
  ;Create uninstaller
  WriteUninstaller "$INSTDIR\Uninstall.exe"
  
  ;Add shortcuts to Start Menu
  !insertmacro MUI_STARTMENU_WRITE_BEGIN Application
    CreateDirectory "$SMPROGRAMS\$StartMenuFolder"
    CreateShortcut "$SMPROGRAMS\$StartMenuFolder\Uninstall Freedom Land.lnk" "$INSTDIR\Uninstall.exe"
    CreateShortcut "$SMPROGRAMS\$StartMenuFolder\Freedom Land.lnk" "$INSTDIR\FreedomLand.exe"
  !insertmacro MUI_STARTMENU_WRITE_END
  
  ;Registry information for add/remove programs
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\Freedom Land" "DisplayName" "Freedom Land"
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\Freedom Land" "UninstallString" "$\"$INSTDIR\Uninstall.exe$\""
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\Freedom Land" "DisplayIcon" "$\"$INSTDIR\icon.ico$\""
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\Freedom Land" "Publisher" "Volk_Milit (aka Ja'Virr-Dar)"
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\Freedom Land" "DisplayVersion" "${VERSION}"
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\Freedom Land" "URLInfoAbout" "https://flmmo.ru"
  WriteRegDWORD HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\Freedom Land" "EstimatedSize" "56832"
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\Freedom Land" "Comments" "������� ��� ���� �� ������� Freedom Land"

SectionEnd

;--------------------------------
;Installer Functions

Function .onInit

  !insertmacro MUI_LANGDLL_DISPLAY

FunctionEnd

;--------------------------------
;Uninstaller Section

Section "Uninstall"

  ;Remove app files
  RMDir /r "$INSTDIR"
  
  ;Remove shortcuts from Start Menu
  !insertmacro MUI_STARTMENU_GETFOLDER Application $StartMenuFolder
  Delete "$SMPROGRAMS\$StartMenuFolder\Uninstall Freedom Land.lnk"
  Delete "$SMPROGRAMS\$StartMenuFolder\Freedom Land.lnk"
  RMDir "$SMPROGRAMS\$StartMenuFolder"

  ;Clear register
  DeleteRegKey HKCU "Software\Freedom Land"
  
  ;Remove uninstaller information from the registry
  DeleteRegKey HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\Freedom Land"

SectionEnd

;--------------------------------
;Uninstaller Functions

Function un.onInit

  !insertmacro MUI_UNGETLANGUAGE
  
FunctionEnd
