QT += core gui

CONFIG += c++14


greaterThan(QT_MAJOR_VERSION, 4): QT += widgets network

TARGET = FreedomLand
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

include(third-party/SingleApplication/singleapplication.pri)
DEFINES += QAPPLICATION_CLASS=QApplication

LIBS += /home/volk/Projects/tes3mp-freedomland/0.7/CrabNet/build/lib/libRakNetLibStatic.a
INCLUDEPATH += /home/volk/Projects/tes3mp-freedomland/0.7/CrabNet/build/include/raknet

unix: LIBS += -larchive

win32 {
    LIBS += path/to/libzlibstatic.a
    LIBS += path/to/libarchive_static.a
    LIBS += -lz
    LIBS += -lws2_32
    INCLUDEPATH += path/to/zlib-1.2.11
    INCLUDEPATH += path/to/libarchive-3.3.1/libarchive
}

SOURCES += src/main.cpp \
    src/core/processinvoker.cpp \
    src/ui/advancedsettingsdialog.cpp \
    src/ui/customkeysequence.cpp \
    src/ui/morrowindinstallationform.cpp \
    src/ui/newmainform.cpp \
    #src/ui/installmasterform.cpp \
    src/ui/newsitem.cpp \
    #src/ui/installitem.cpp \
    src/ui/aboutform.cpp \
    src/core/apiparser.cpp \
    src/core/settings.cpp \
    src/core/checksetup.cpp \
    src/core/paths.cpp \
    src/core/packages.cpp \
    src/ui/progressform.cpp \
    #src/ui/supersecretdialog.cpp \
    src/utils/archives.cpp \
    src/utils/network.cpp \
    src/utils/hash.cpp \
    src/utils/converter.cpp \
    src/utils/crabnetinterface.cpp \
    third-party/ftlip/QT/ftlip.cpp \
    third-party/qt-crc32/crc32.cpp

FORMS += \
    src/ui/advancedsettingsdialog.ui \
    src/ui/morrowindinstallationform.ui \
    src/ui/newmainform.ui \
    #src/ui/installmasterform.ui \
    src/ui/aboutform.ui \
    src/ui/progressform.ui \
    #src/ui/supersecretdialog.ui

HEADERS += \
    src/core/processinvoker.h \
    src/ui/advancedsettingsdialog.h \
    src/ui/customkeysequence.h \
    src/ui/morrowindinstallationform.h \
    src/ui/newmainform.h \
    #src/ui/installmasterform.h \
    src/ui/newsitem.h \
    #src/ui/installitem.h \
    src/ui/aboutform.h \
    src/core/checksetup.h \
    src/core/paths.h \
    src/core/apiparser.h \
    src/core/settings.h \
    src/core/packages.h \
    src/ui/progressform.h \
    #src/ui/supersecretdialog.h \
    src/utils/archives.h \
    src/utils/network.h \
    src/utils/hash.h \
    src/utils/converter.h \
    src/utils/crabnetinterface.h \
    third-party/ftlip/QT/ftlip.h \
    third-party/qt-crc32/crc32.h

RESOURCES += \
    resources.qrc

win32: RC_ICONS = fl_logo.ico
